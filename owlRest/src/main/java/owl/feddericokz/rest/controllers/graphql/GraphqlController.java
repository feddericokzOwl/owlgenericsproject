package owl.feddericokz.rest.controllers.graphql;

import graphql.GraphQL;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import owl.feddericokz.rest.controllers.graphql.support.GraphqlControllerSupport;

@Component
public class GraphqlController extends GraphqlControllerSupport {

    /*
     * Fields
     */

    private GraphQL graphQL;

    /*
     * Constructors
     */

    public GraphqlController() {
        super("schema.graphqls");
    }

    /*
     * Methods
     */

    @Override
    public void setGraphql(GraphQL graphQL) {
        this.graphQL=graphQL;
    }

    /*
     * Beans
     */

    @Bean
    public GraphQL graphQL() {
        return graphQL;
    }

}

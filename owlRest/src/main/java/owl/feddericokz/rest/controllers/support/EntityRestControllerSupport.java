package owl.feddericokz.rest.controllers.support;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import owl.feddericokz.rest.controllers.EntityRestController;
import owl.feddericokz.system.domains.EntityVo;
import owl.feddericokz.system.domains.ServiceReady;
import owl.feddericokz.system.exceptions.EmptyResourceException;
import owl.feddericokz.system.exceptions.EntityNotFoundException;
import owl.feddericokz.system.exceptions.InvalidEntityException;
import owl.feddericokz.system.exceptions.StaleResourceException;
import owl.feddericokz.system.services.EntityService;
import owl.feddericokz.system.utils.MergingUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.net.URI;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class EntityRestControllerSupport<S extends EntityVo<T,ID>,T extends ServiceReady<ID>, ID extends Serializable> implements EntityRestController<S,T,ID> {

    /*
     * Constants
     */

    public static final Integer DEFAULT_SIZE = 5;
    public static final Integer DEFAULT_PAGE = 0;
    public static final List<String> DEFAULT_SORT_DIR = new ArrayList<>();


    /*
     * Fields
     */

    @Autowired
    private EntityService<T,ID> entityService;

    // Debug
    @PersistenceContext
    private EntityManager entityManager;

    /*
     * Constructor
     */

    public EntityRestControllerSupport() {
    }

    /*
     * Mandatory Methods
     */

    @Override
    public ResponseEntity<S> getResourceById(ID id) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String optimisticLock = stripQuotesFromString(request.getHeader("If-None-Match"));
        S entity = null;
        try {
            entity = entityService.findById(id);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.noContent().build();
        }
        if (optimisticLock==null || !optimisticLock.equals(entity.getVersionTag().format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS")))) {
            return ResponseEntity.ok()
                    .headers(new HeadersBuilder().contentType(MediaType.APPLICATION_JSON)
                    .etag(entity.getVersionTag().format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS"))).build())
                    .body(entity);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_MODIFIED)
                    .headers(new HeadersBuilder()
                        .etag(entity.getVersionTag().format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS")))
                        .build())
                    .build();
        }
    }

    @Override
    public ResponseEntity<List<S>> getAll(List<String> sort, List<String> sortDir) {
        if (areParamsValid(sort,sortDir)) {
            List<S> all;
            try {
                if (sort != null) {
                    Sort finalSort = assembleSort(sort, sortDir);
                    all = new ArrayList<>(entityService.findAll(finalSort));
                } else {
                    all = new ArrayList<>(entityService.findAll());
                }
            } catch (EmptyResourceException e) {
                return ResponseEntity.noContent().build();
            }
            return ResponseEntity.ok()
                    .headers(new HeadersBuilder().contentType("application/json;charset=UTF-8").build())
                    .body(all);
        } else {
            String url = stripInvalidParams(sort,sortDir);
            return ResponseEntity.status(HttpStatus.FOUND)
                    .headers(new HeadersBuilder().location(url).build())
                    .build();
        }
    }

    @Override
    public ResponseEntity<Page<S>> getAll(Integer page, Integer size, List<String> sort, List<String> sortDir) {

        //If size is present, and page is not, page must be default.
        if (size != null && page == null) {
            page = DEFAULT_PAGE;
        }
        //If page is present, and size is not, size must be default.
        if (page != null && size == null) {
            size = DEFAULT_SIZE;
        }
        if (areParamsValid(sort,sortDir,page,size)) {
            Page<S> fetchPage = null;
            try {
                fetchPage = entityService.findAll(toPageable(page,size,sort,sortDir));
            } catch (EmptyResourceException e) {
                return ResponseEntity.noContent().build();
            }
            return ResponseEntity.ok()
                    .headers(new HeadersBuilder().contentType("application/json;charset=UTF-8").build())
                    .body(fetchPage);
        } else {
            String url = stripInvalidParams(sort,sortDir,page,size);
            return ResponseEntity.status(HttpStatus.FOUND)
                    .headers(new HeadersBuilder().location(url).build())
                    .build();
        }
    }

    @Override
    public ResponseEntity<?> createResource(S entity) {
        S savedEntity = null;
        try {
            savedEntity = entityService.save(entity);
        } catch (InvalidEntityException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (StaleResourceException e) {
            return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).build();
        }
        String baseUri = ServletUriComponentsBuilder.fromCurrentRequest().toUriString();
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(baseUri+"/{id}");
        URI newURI = builder.buildAndExpand(savedEntity.getId()).toUri();
        return ResponseEntity.created(newURI)
                .headers(new HeadersBuilder().contentType(MediaType.APPLICATION_JSON)
                    .etag(savedEntity.getVersionTag().format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS"))).build())
                .body(savedEntity);
    }

    @Override
    public ResponseEntity<?> updateResource(ID id, S updateBag) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return handleUpdate(id,updateBag,request, MergingUtils.FULL);
    }

    private ResponseEntity<?> handleUpdate(ID id, S updateBag, HttpServletRequest request, String typeOfMerge) {
        String optimisticLock = stripQuotesFromString(request.getHeader("If-Match"));
        if (optimisticLock==null) optimisticLock = stripQuotesFromString(request.getHeader("Etag")); // Maybe search for Etag header if "If-Match" is not present.
        if (optimisticLock!=null) {
            S entity = null;
            try {
                entity = entityService.findById(id);
            } catch (EntityNotFoundException e) {
                return ResponseEntity.noContent().build();
            }
            String localLock = entity.getVersionTag().format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS"));
            if (!localLock.equals(optimisticLock))
                return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).headers((new HeadersBuilder()).contentType(MediaType.APPLICATION_JSON).etag(entity.getVersionTag().format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS"))).build()).body(entity);
            S updatedEntity = MergingUtils.mergeVo(entity,updateBag,typeOfMerge);
            try {
                updatedEntity = entityService.save(updatedEntity);
            } catch (InvalidEntityException e) {
                return ResponseEntity.badRequest().body(e.getMessage());
            } catch (StaleResourceException e) {
                return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(entity);
            }
            return ResponseEntity.ok()
                    .headers(new HeadersBuilder().contentType(MediaType.APPLICATION_JSON)
                        .etag(updatedEntity.getVersionTag().format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS"))).build())
                    .body(updatedEntity);
        } else {
            return ResponseEntity.badRequest().body("Concurrency Control header is missing. [Etag, If-Match]");
        }
    }

    @Override
    public ResponseEntity<?> partialUpdateResource(ID id, S updateInfo) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        return handleUpdate(id,updateInfo,request, MergingUtils.PARTIAL);
    }

    @Override
    public ResponseEntity<?> deleteResource(ID id) {
        try {
            this.entityService.deleteById(id);
        } catch (EntityNotFoundException var3) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(var3.getMessage());
        }

        return ResponseEntity.noContent().build();
    }


    /*
     * Methods
     */

    protected Class<T> getType() {
        ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
        return (Class<T>) type.getActualTypeArguments()[0];
    }

    protected String stripQuotesFromString(String s) {
        if (s!=null) {
            if (s.startsWith("\"") && s.endsWith("\"")) {
                return s.substring(1, s.lastIndexOf("\""));
            }
            return s;
        }
        return null;
    }

    protected Boolean areParamsValid(List<String> sort, List<String> sortDir) {
        Class<?> type = getType();
        Field[] fields = type.getDeclaredFields();
        Set<String> params = new HashSet<>();
        if (fields.length!=0) {
            for (Field field: fields) {
                params.add(field.getName());
            }
        }
        if (sort!=null) {
            for (String s : sort) {
                if (!params.contains(s)) return false;
            }
        }
        if (sortDir!=null) {
            for (String s : sortDir) {
                if (!params.contains(s.substring(0, s.indexOf(".")))) return false;
            }
        }
        return true;
    }

    protected Boolean areParamsValid(List<String> sort, List<String> sortDir, Integer page, Integer size) {
        if (size == null || page == null) return false;
        if (size < 1) return false;
        if (size > 200) return false;
        if (page < 0) return false;
        Long totalItems = entityService.countAll();
        Long totalPages = (totalItems/size) <= 1 ? 0 : (totalItems/size) -1; //Cause pages start at 0.
        if ((totalItems%size)>0) totalPages++;
        if (page > totalPages) return false;
        return areParamsValid(sort,sortDir);
    }

    protected String stripInvalidParams(List<String> sort, List<String> sortDir) {
        Class<?> type = getType();
        Field[] fields = type.getDeclaredFields();
        Set<String> params = new HashSet<>();
        if (fields.length!=0) {
            for (Field field: fields) {
                params.add(field.getName());
            }
        }
        List<String> stripedSort = new ArrayList<>();
        List<String> stripedSortDir = new ArrayList<>();
        if (sort!=null) {
            for (String s : sort) {
                if (params.contains(s)) {
                    stripedSort.add(s);
                }
            }
        }
        if (sortDir!=null) {
            for (String s : sortDir) {
                if (params.contains(s.substring(0, s.indexOf(".")))) {
                    stripedSortDir.add(s);
                }
            }
        }
        String baseUri = ServletUriComponentsBuilder.fromCurrentRequest().toUriString();
        boolean containsParams = baseUri.contains("?");
        StringBuilder strippedUri = null;
        if (containsParams) {
            strippedUri = new StringBuilder(baseUri.substring(0, baseUri.lastIndexOf("?")));
        } else {
            strippedUri = new StringBuilder(baseUri);
        }
        if (!stripedSort.isEmpty()) {
            strippedUri.append("?sort=");
            boolean isFirstArgument = true;
            for (String s : stripedSort) {
                if (isFirstArgument) {
                    strippedUri.append(s);
                    isFirstArgument = false;
                } else {
                    strippedUri.append(",").append(s);
                }
            }
        }
        if (!stripedSortDir.isEmpty()) {
            strippedUri.append("&sortDir=");
            boolean isFirstArgument = true;
            for (String s : stripedSortDir) {
                if (isFirstArgument) {
                    strippedUri.append(s);
                    isFirstArgument = false;
                } else {
                    strippedUri.append(",").append(s);
                }
            }
        }
        return strippedUri.toString();
    }

    protected String stripInvalidParams(List<String> sort, List<String> sortDir, Integer page, Integer size) {
        String strippedUri = stripInvalidParams(sort,sortDir);
        if (size == null) size = DEFAULT_SIZE;
        if (page == null) page = DEFAULT_PAGE;
        Integer correctedSize = size;
        Integer correctedPage = page;
        if (size < 1) correctedSize = 1;
        if (size > 200) correctedSize = 200;
        if (page < 0) correctedPage = 0;
        Long totalItems = entityService.countAll();
        Long totalPages = (totalItems/size) <= 1 ? 0 : (totalItems/size) -1; //Cause pages start at 0.
        if ((totalItems%size)>0) totalPages++;
        if (page > totalPages) correctedPage = totalPages.intValue();
        return !strippedUri.contains("?")
                ? strippedUri+"?page="+correctedPage+"&size="+correctedSize
                : strippedUri+"&page="+correctedPage+"&size="+correctedSize;
    }

    protected static Sort assembleSort(List<String> sort, List<String> sortDir) {
        Objects.requireNonNull(sort,"Parameter sort cannot be null!");
        List<Sort> sortList = sort.stream()
                .map((sortParameter) -> {
                    String sortParameterDesc = sortParameter+".desc";
                    Set set = new HashSet(sortDir == null ? DEFAULT_SORT_DIR : sortDir);
                    Sort.Direction direction;
                    if (set.contains(sortParameterDesc)) {
                        direction = Sort.Direction.DESC;
                    } else {
                        direction = Sort.Direction.ASC;
                    }
                    return Sort.by(direction,sortParameter);
                }).collect(Collectors.toList());
        Sort firstSort = sortList.get(0);
        Sort finalSort = firstSort;
        for (int i=1; i<=sortList.size()-1;i++) {
            firstSort.and(sortList.get(i));
            finalSort = firstSort.and(sortList.get(i));
        }
        return finalSort;
    }

    protected static Pageable toPageable(Integer page, Integer size, List<String> sort, List<String> sortDir) {
        page = page == null ? DEFAULT_PAGE : page;
        size = size == null ? DEFAULT_SIZE : size;
        if (sort == null) {
            return PageRequest.of(page,size);
        } else {
            Sort finalSort = assembleSort(sort, sortDir);
            return PageRequest.of(page,size,finalSort);
        }
    }

    /*
     * Inner Classes
     */

    public static class HeadersBuilder {

        /*
         * Fields
         */

        private HttpHeaders httpHeaders = new HttpHeaders();

        /*
         * Constructors
         */

        public HeadersBuilder() {
        }

        /*
         * Methods
         */

        public HttpHeaders build() {
            return httpHeaders;
        }

        public HeadersBuilder contentType(String type) {
            httpHeaders.set("Content-Type",type);
            return this;
        }

        public HeadersBuilder contentType(MediaType mediaType) {
            httpHeaders.setContentType(mediaType);
            return this;
        }

        public HeadersBuilder etag(String etag) {
            httpHeaders.set("Etag", etag);
            // httpHeaders.set("Etag","\""+etag+"\""); // Why does this have those quotes?
            return this;
        }

        public HeadersBuilder location(String url) {
            httpHeaders.set("Location",url);
            return this;
        }

        public HeadersBuilder publicKey(String key){
            httpHeaders.set("Public-Key",key);
            return this;
        }

    }

    @Data
    public static class OwlPage {

        /*
         * Fields
         */

        private List<?> content;
        private Integer number;
        private Integer size;
        private Integer totalPages;
        private Integer totalElements;
        private Integer numberOfElements;
        private Boolean first;
        private Boolean last;
        private Boolean empty;

        /*
         * Constructors
         */

        public OwlPage(PagedListHolder pagedListHolder){
            this.content=pagedListHolder.getPageList();
            this.number =pagedListHolder.getPage();
            this.size =pagedListHolder.getPageSize();
            this.totalPages =pagedListHolder.getPageCount();
            this.totalElements=pagedListHolder.getSource().size();
            this.numberOfElements=pagedListHolder.getNrOfElements();
            this.first=pagedListHolder.isFirstPage();
            this.last=pagedListHolder.isLastPage();
            this.empty=pagedListHolder.getPageList().isEmpty();
        }
    }

}

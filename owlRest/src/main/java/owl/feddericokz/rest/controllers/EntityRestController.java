package owl.feddericokz.rest.controllers;


import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import owl.feddericokz.system.domains.EntityVo;
import owl.feddericokz.system.domains.ServiceReady;

import java.io.Serializable;
import java.util.List;

/**
 * Generic Rest Controller with basic CRUD operations.
 * All methods may return an HTTP_STATUS.UNAUTHORIZED(401) if the user has not been authenticated,
 * or HTTP_STATUS.FORBIDDEN(403) if the user doesnt have permission to execute the task.
 *
 * @param <S> Dto Type the controller will return.
 * @param <T> Entity type the controller handles.
 * @param <ID> Type of the id of the entity the controller handles.
 */
public interface EntityRestController<S extends EntityVo<T, ID>, T extends ServiceReady<ID>, ID extends Serializable> {

    /**
     * Fetches a requested resource by its Id.
     *
     * @param id Identifier of the requested resource.
     * @return Returns HTTP_STATUS.OK(200) with the requested entity in the message body.
     * Or HTTP_STATUS.NOT_FOUND(404) if the given Id doesnt not belong to any resource on the server.
     */
    @GetMapping("/{id}")
    ResponseEntity<S> getResourceById(@PathVariable(value = "id") ID id);

    /**
     * Fetches all the resources of a given type.
     *
     * @param sort    Name of the parameter we want to sort by the resultSet by.
     * @param sortDir Direction we want the resultSet to be sorted by, in the form of
     *                paramName.asc or paramName.desc
     * @return Returns HTTP_STATUS.OK(200) with a list of entities in the message body.
     * OR HTTP_STATUS.NO_CONTENT(204) in case the resource is empty.
     */
    @GetMapping
    ResponseEntity<List<S>> getAll(@RequestParam(required = false, value = "sort") List<String> sort, @RequestParam(required = false, value = "sortDir") List<String> sortDir);

    /**
     * Fetches all the resources of a given type and accepts pagination parameters.
     *
     * @param page    Number of the page you want to fetch.
     * @param size    Size of the page you want to fetch.
     * @param sort    Name of the parameter we want to sort by the resultSet by
     * @param sortDir Direction we want the resultSet to be sorted by, in the form of
     *                paramName.asc or paramName.desc
     * @return Returns HTTP_STATUS.OK(200) with a list of entities in the message body.
     * OR HTTP_STATUS.NO_CONTENT(204) in case the resource is empty.
     */
    @GetMapping("/paginated")
    ResponseEntity<Page<S>> getAll(@RequestParam(required = false, value = "page") Integer page,
                                   @RequestParam(required = false, value = "size") Integer size,
                                   @RequestParam(required = false, value = "sort") List<String> sort,
                                   @RequestParam(required = false, value = "sortDir") List<String> sortDir);

    /**
     * Creates a resource of the corresponding type.
     *
     * @param entity Is the entity to be created in the server.
     * @return Returns HTTP_STATUS.CREATED(201) if the resource has been created successfully.
     * or HTTP_STATUS.BAD_REQUEST(400) if the entity was not a valid one and the server refused to create it.
     */
    @PostMapping
    ResponseEntity<?> createResource(@RequestBody S entity);

    /**
     * Makes a full update of the resource with the given Id.
     *
     * @param id Identifier of the entity to be updated.
     * @param entity Object carrying the values to be updated.
     * @return Returns HTTP_STATUS.OK(200) if the resource has been successfully updated,
     * or HTTP_STATUS.NOT_FOUND(404) if the given Id doesnt belong to any resource on the server,
     * or HTTP_STATUS.PRECONDITION_FAILED(412) if the client had a stale copy of the resource its trying to update.
     */
    @PutMapping("/{id}")
    ResponseEntity<?> updateResource(@PathVariable(value = "id") ID id, @RequestBody S entity);

    /**
     * Makes a partial update of the resource with the given Id.
     *
     * @param id Identifier of the entity to be updated.
     * @param entity Object carrying the values to be updated.
     * @return Returns HTTP_STATUS.OK(200) if the resource has been successfully updated,
     * or HTTP_STATUS.NOT_FOUND(404) if the given Id doesnt belong to any resource on the server,
     * or HTTP_STATUS.PRECONDITION_FAILED(412) if the client had a stale copy of the resource its trying to update.
     */
    @PatchMapping("/{id}")
    ResponseEntity<?> partialUpdateResource(@PathVariable(value = "id") ID id, @RequestBody S entity);

    /**
     * Deletes a resource on the server.
     *
     * @param id Identifier of the entity to be deleted.
     * @return Returns HTTP_STATUS.NO_CONTENT(204) if the resource has been deleted successfully,
     * or HTTP_STATUS.NOT_FOUND(404) if the given Id doesnt belong to any resource on the server.
     */
    @DeleteMapping("/{id}")
    ResponseEntity<?> deleteResource(@PathVariable("id") ID id);

}

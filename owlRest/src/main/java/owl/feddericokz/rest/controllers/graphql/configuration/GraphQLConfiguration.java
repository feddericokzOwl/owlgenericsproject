package owl.feddericokz.rest.controllers.graphql.configuration;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import owl.feddericokz.rest.controllers.graphql.GraphqlController;

@Configuration
@ComponentScan(basePackageClasses = {GraphqlController.class})
public class GraphQLConfiguration {
}

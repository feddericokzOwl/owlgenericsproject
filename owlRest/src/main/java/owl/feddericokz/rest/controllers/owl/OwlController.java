package owl.feddericokz.rest.controllers.owl;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/owl")
public interface OwlController {

    @GetMapping("/whatDidTheOwlSay")
    ResponseEntity<String> whatDidTheOwlSay();

}

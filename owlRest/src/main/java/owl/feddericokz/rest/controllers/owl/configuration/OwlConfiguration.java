package owl.feddericokz.rest.controllers.owl.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import owl.feddericokz.rest.controllers.owl.support.OwlControllerSupport;

@Configuration
@ComponentScan(basePackageClasses = { OwlControllerSupport.class })
public class OwlConfiguration {
}

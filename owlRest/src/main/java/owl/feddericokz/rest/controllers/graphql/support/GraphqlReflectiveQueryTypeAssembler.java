package owl.feddericokz.rest.controllers.graphql.support;

import graphql.language.FieldDefinition;
import graphql.language.TypeDefinition;
import org.springframework.util.Assert;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class GraphqlReflectiveQueryTypeAssembler {

    public static void joinQueryTypes(TypeDefinition queryTypeDefinition, TypeDefinition updateTypeDefinition) throws NoSuchFieldException, IllegalAccessException {
        Assert.isTrue(queryTypeDefinition.getName().equals("Query"),"Must be \"Query\" typeDefinition");
        Assert.isTrue(updateTypeDefinition.getName().equals("Query"),"Must be \"Query\" typeDefinition");
        Class clazz = queryTypeDefinition.getClass();
        Field fieldDefinitionsField = clazz.getDeclaredField("fieldDefinitions");
        fieldDefinitionsField.setAccessible(true);
        ArrayList<FieldDefinition> queryFieldDefinitionsList = (ArrayList<FieldDefinition>) fieldDefinitionsField.get(queryTypeDefinition);
        ArrayList<FieldDefinition> updateFieldDefinitionsList = (ArrayList<FieldDefinition>) fieldDefinitionsField.get(updateTypeDefinition);
        queryFieldDefinitionsList.addAll(updateFieldDefinitionsList);
        fieldDefinitionsField.set(queryTypeDefinition,queryFieldDefinitionsList);
    }

}

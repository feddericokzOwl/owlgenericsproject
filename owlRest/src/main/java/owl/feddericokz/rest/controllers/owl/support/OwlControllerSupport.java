package owl.feddericokz.rest.controllers.owl.support;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import owl.feddericokz.rest.controllers.owl.OwlController;

@RestController
public class OwlControllerSupport implements OwlController {

    /*
     * Methods
     */

    @Override
    public ResponseEntity<String> whatDidTheOwlSay() {
        return ResponseEntity.ok("Hoo-Hoo!");
    }

}

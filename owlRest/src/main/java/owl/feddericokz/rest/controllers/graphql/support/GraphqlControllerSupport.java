package owl.feddericokz.rest.controllers.graphql.support;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import graphql.GraphQL;
import graphql.language.Definition;
import graphql.language.SDLDefinition;
import graphql.language.TypeDefinition;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.*;
import org.springframework.beans.factory.annotation.Autowired;
import owl.feddericokz.system.graphql.GraphqlDefinitionsProvider;
import owl.feddericokz.system.graphql.GraphqlRunTimeWiringProvider;
import owl.feddericokz.system.graphql.support.GraphqlDateScalar;

import javax.annotation.PostConstruct;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static owl.feddericokz.rest.controllers.graphql.support.GraphqlReflectiveQueryTypeAssembler.joinQueryTypes;

public abstract class GraphqlControllerSupport {

    /*
     * Fields
     */

    private String schemaUrlString;

    /*
     * Services
     */

    @Autowired
    private List<GraphqlDefinitionsProvider> graphqlDefinitionsProviderList;
    @Autowired
    private List<GraphqlRunTimeWiringProvider> graphqlRunTymeWiringProviderList;

    /*
     * Constructor
     */

    public GraphqlControllerSupport(String schemaUrlString) {
        this.schemaUrlString = schemaUrlString;
    }

    /*
     * Methods
     */

    protected GraphQLSchema buildSchema(String sdlQuery) throws Exception {
        TypeDefinitionRegistry typeDefinitionRegistry = new SchemaParser().parse(sdlQuery);
        List<Definition> definitionList = new ArrayList<>();
        for (GraphqlDefinitionsProvider graphqlDefinitionsProvider : graphqlDefinitionsProviderList) {
            definitionList.addAll(graphqlDefinitionsProvider.getDefinitions());
        }
        for (Definition definition : definitionList) {
            if (((TypeDefinition) definition).getName().equals("Query")) {
                joinQueryTypes(typeDefinitionRegistry.getType("Query").get(), (TypeDefinition) definition);
            } else typeDefinitionRegistry.add((SDLDefinition) definition);
        }
        RuntimeWiring runtimeWiring = buildWiring();
        SchemaGenerator schemaGenerator = new SchemaGenerator();
        return schemaGenerator.makeExecutableSchema(typeDefinitionRegistry,runtimeWiring);
    }

    protected RuntimeWiring buildWiring() {
        RuntimeWiring.Builder builder = RuntimeWiring.newRuntimeWiring();
        List<TypeRuntimeWiring> runtimeWiringsList = new ArrayList<>();
        for (GraphqlRunTimeWiringProvider graphqlRunTymeWiringProvider : graphqlRunTymeWiringProviderList) {
            runtimeWiringsList.addAll(graphqlRunTymeWiringProvider.getTypeRuntimeWirings());
        }
        for (TypeRuntimeWiring typeRuntimeWiring : runtimeWiringsList) {
            builder.type(typeRuntimeWiring);
        }
        builder.scalar(new GraphqlDateScalar());
        return builder.build();
    }

    /*
     * Init
     */

    @PostConstruct
    protected void setUp() throws Exception {
        URL schemaUrl = Resources.getResource(schemaUrlString);
        String sdlQuery = Resources.toString(schemaUrl, Charsets.UTF_8);
        this.setGraphql(GraphQL.newGraphQL(buildSchema(sdlQuery)).build());
    }

    /*
     * Abstract
     */

    public abstract void setGraphql(GraphQL graphQL);

}

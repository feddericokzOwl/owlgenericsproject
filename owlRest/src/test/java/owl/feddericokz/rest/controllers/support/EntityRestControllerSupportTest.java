package owl.feddericokz.rest.controllers.support;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.*;
import org.springframework.util.MultiValueMap;
import owl.feddericokz.rest.configurations.TestSystemConfiguration;
import owl.feddericokz.testSystem.TestAppContextConfiguration;
import owl.feddericokz.testSystem.domains.InmutableCompositeClassVo;
import owl.feddericokz.testSystem.domains.MutableCompositeClassVo;
import owl.feddericokz.testSystem.domains.SimpleClassAVo;
import owl.feddericokz.testSystem.domains.SimpleClassBVo;
import owl.feddericokz.testSystem.services.SimpleClassBService;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.time.format.DateTimeFormatter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@Import(TestSystemConfiguration.class)
@SpringBootTest(
        classes = TestAppContextConfiguration.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EntityRestControllerSupportTest {

    /*
     * Fields
     */

    private String url;

    /*
     * Services
     */

    @Autowired
    private SimpleClassBService simpleClassBService;
    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate restTemplate;

    /*
     * Configuration
     */

    @PostConstruct
    private void init() {
        url = "http://localhost:" + port;
    }

    /*
     * Methods
     */

    @Test
    public void areYouThereOwl() throws Exception {

        assertThat(restTemplate.getForObject(url + "/owl/whatDidTheOwlSay", String.class) , equalTo("Hoo-Hoo!"));

    }

    @Test
    @Order(1)
    public void createResource_test() throws Exception {

        // Setup
        SimpleClassBVo myBVo = SimpleClassBVo.builder()
                .someValueInt(3569)
                .someValueString("HelloNoob")
                .build();

        simpleClassBService.save(myBVo);

        InmutableCompositeClassVo myInmutableVo = InmutableCompositeClassVo.builder()
                .classB(simpleClassBService.findById(1L))  // Sneaky Throws
                .someValueInt(897)
                .someValueString("ShitNigga")
                .build();

        SimpleClassAVo myAVo = SimpleClassAVo.builder()
                .someValueInt(3569)
                .someValueString("HelloNoob")
                .build();

        MutableCompositeClassVo myMutableVo = MutableCompositeClassVo.builder()
                .someValueInt(456)
                .someValueString("This is going well")
                .simpleClassA(myAVo)
                .inmutableClass(myInmutableVo)
                .build();

        // Excecution

        HttpEntity<MutableCompositeClassVo> httpEntity = new HttpEntity<>(myMutableVo);
        ResponseEntity<MutableCompositeClassVo> response = restTemplate.exchange(url + "/api/resources/mutable-domain", HttpMethod.POST, httpEntity, MutableCompositeClassVo.class);

        // Assertions.

        assertThat(response.getStatusCode(),equalTo(HttpStatus.CREATED));

        assertThat(response.getHeaders().getLocation(), equalTo(new URI(url+"/api/resources/mutable-domain/"+response.getBody().getId())));

        assertThat(response.getHeaders().getETag().replace("\"",""), equalTo(response.getBody().getVersionTag().format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS"))));

        assertThat(response.getHeaders().getContentType(), equalTo(MediaType.APPLICATION_JSON));

        // I can test the creation with an invalid object here.

        myMutableVo.setInmutableClass(null);

        HttpEntity<MutableCompositeClassVo> httpEntity2 = new HttpEntity<>(myMutableVo);
        ResponseEntity<String> response2 = restTemplate.exchange(url + "/api/resources/mutable-domain", HttpMethod.POST, httpEntity2, String.class);

        assertThat(response2.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        assertThat(response2.getBody(), equalTo("Entity of type: [MutableCompositeClass] failed validation."));

    }

    @Test
    @Order(2)
    public void fullUpdateResource_test() {

        // Set up

        ResponseEntity<MutableCompositeClassVo> response1 = restTemplate.getForEntity(url + "/api/resources/mutable-domain/1", MutableCompositeClassVo.class);

        assertThat(response1.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response1.getHeaders().getETag().replace("\"",""), equalTo(response1.getBody().getVersionTag().format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS"))));
        assertThat(response1.getBody().getId(), equalTo(1L));
        assertThat(response1.getHeaders().getContentType(), equalTo(MediaType.APPLICATION_JSON));

        MutableCompositeClassVo myMutableVo = response1.getBody();

        myMutableVo.setSomeValueString("Changed through rest layer.");

        // Execution.

        MultiValueMap<String, String> headersMap = new HttpHeaders();
        headersMap.add("If-Match", myMutableVo.getVersionTag().format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS")));
        HttpEntity<MutableCompositeClassVo> httpEntity = new HttpEntity<>(myMutableVo, headersMap);
        ResponseEntity<MutableCompositeClassVo> response2 = restTemplate.exchange(url + "/api/resources/mutable-domain/1", HttpMethod.PUT, httpEntity, MutableCompositeClassVo.class);

        assertThat(response2.getStatusCode(), equalTo(HttpStatus.OK));

        assertThat(response2.getHeaders().getETag().replace("\"",""), equalTo(response2.getBody().getVersionTag().format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS"))));

        assertThat(response2.getHeaders().getContentType(), equalTo(MediaType.APPLICATION_JSON));

        assertThat(response2.getBody().getSomeValueString(), equalTo(myMutableVo.getSomeValueString()));

        assertThat(response2.getBody().getVersionTag(), not(equalTo(myMutableVo.getVersionTag())));

        // Now i cant test it failing.

        myMutableVo.setSomeValueString("Change this one more time.");

        ResponseEntity<MutableCompositeClassVo> response3 = restTemplate.exchange(url + "/api/resources/mutable-domain/1", HttpMethod.PUT, httpEntity, MutableCompositeClassVo.class);

        assertThat(response3.getStatusCode(), equalTo(HttpStatus.PRECONDITION_FAILED));

        assertThat(response3.getBody().getId(), equalTo(myMutableVo.getId()));

        assertThat(response3.getBody().getSomeValueString(), not(equalTo(myMutableVo.getSomeValueString())));

        // Now i can make it invalid.

        MutableCompositeClassVo invalidMutableVo = response3.getBody();

        invalidMutableVo.setInmutableClass(null);

        MultiValueMap<String, String> headersMap2 = new HttpHeaders();
        headersMap2.add("If-Match", invalidMutableVo.getVersionTag().format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS")));
        HttpEntity<MutableCompositeClassVo> httpEntity2 = new HttpEntity<>(invalidMutableVo, headersMap2);
        ResponseEntity<String> response4 = restTemplate.exchange(url + "/api/resources/mutable-domain/1", HttpMethod.PUT, httpEntity2, String.class);

        assertThat(response4.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        assertThat(response4.getBody(), equalTo("Entity of type: [MutableCompositeClass] failed validation."));

    }

    @Test
    @Order(3)
    public void partialUpdateResource_test() {

        // Set up

        MutableCompositeClassVo updateVo = MutableCompositeClassVo.builder().someValueString("Partial Updated.").build();

        ResponseEntity<MutableCompositeClassVo> response1 = restTemplate.getForEntity(url + "/api/resources/mutable-domain/1", MutableCompositeClassVo.class);


        // Execution.

        MultiValueMap<String, String> headersMap = new HttpHeaders();
        headersMap.add("If-Match", response1.getBody().getVersionTag().format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS")));
        HttpEntity<MutableCompositeClassVo> httpEntity = new HttpEntity<>(updateVo, headersMap);
        ResponseEntity<MutableCompositeClassVo> response2 = restTemplate.exchange(url + "/api/resources/mutable-domain/1", HttpMethod.PATCH, httpEntity, MutableCompositeClassVo.class);

        assertThat(response2.getStatusCode(), equalTo(HttpStatus.OK));

        assertThat(response2.getBody().getSomeValueInt(), notNullValue());

        assertThat(response2.getBody().getSomeValueString(), equalTo(updateVo.getSomeValueString()));

        // Lets try not sending the concurrency control header.

        MultiValueMap<String, String> headersMap2 = new HttpHeaders();
        HttpEntity<MutableCompositeClassVo> httpEntity2 = new HttpEntity<>(updateVo, headersMap2);
        ResponseEntity<String> response3 = restTemplate.exchange(url + "/api/resources/mutable-domain/1", HttpMethod.PATCH, httpEntity2, String.class);

        assertThat(response3.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        assertThat(response3.getBody(), equalTo("Concurrency Control header is missing. [Etag, If-Match]"));

    }

    @Test
    @Order(4)
    public void findResourceById_notModified_test() {

        // Set up

        MutableCompositeClassVo updateVo = MutableCompositeClassVo.builder().someValueString("Partial Updated.").build();

        ResponseEntity<MutableCompositeClassVo> response1 = restTemplate.getForEntity(url + "/api/resources/mutable-domain/1", MutableCompositeClassVo.class);

        // Execution

        MultiValueMap<String, String> headersMap = new HttpHeaders();
        headersMap.add("If-None-Match",response1.getBody().getVersionTag().format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS")));
        HttpEntity<Void> httpEntity = new HttpEntity<>(null, headersMap);
        ResponseEntity<String> response2 = restTemplate.exchange(url + "/api/resources/mutable-domain/1", HttpMethod.GET, httpEntity, String.class);

        assertThat(response2.getStatusCode(), equalTo(HttpStatus.NOT_MODIFIED));

        assertThat(response2.getHeaders().getETag(), equalTo(response1.getBody().getVersionTag().format(DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss.SSS"))));

    }

}

package owl.feddericokz.testSystem.repositories;

import org.springframework.stereotype.Repository;
import owl.feddericokz.system.repositories.EntityRepository;
import owl.feddericokz.testSystem.domains.SimpleClassA;

@Repository
public interface SimpleClassARepository extends EntityRepository<SimpleClassA,Long> {
}

package owl.feddericokz.testSystem.repositories;

import org.springframework.stereotype.Repository;
import owl.feddericokz.system.repositories.EntityRepository;
import owl.feddericokz.testSystem.domains.SimpleClassB;

@Repository
public interface SimpleClassBRepository extends EntityRepository<SimpleClassB,Long> {
}

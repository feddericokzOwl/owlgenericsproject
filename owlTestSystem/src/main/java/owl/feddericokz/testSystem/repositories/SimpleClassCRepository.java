package owl.feddericokz.testSystem.repositories;

import org.springframework.stereotype.Repository;
import owl.feddericokz.system.repositories.EntityRepository;
import owl.feddericokz.testSystem.domains.SimpleClassC;

@Repository
public interface SimpleClassCRepository extends EntityRepository<SimpleClassC,Long> {
}

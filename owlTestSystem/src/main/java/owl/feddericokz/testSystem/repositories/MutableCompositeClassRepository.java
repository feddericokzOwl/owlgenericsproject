package owl.feddericokz.testSystem.repositories;

import org.springframework.stereotype.Repository;
import owl.feddericokz.system.repositories.EntityRepository;
import owl.feddericokz.testSystem.domains.MutableCompositeClass;

@Repository
public interface MutableCompositeClassRepository extends EntityRepository<MutableCompositeClass,Long> {
}

package owl.feddericokz.testSystem.repositories;

import org.springframework.stereotype.Repository;
import owl.feddericokz.system.repositories.EntityRepository;
import owl.feddericokz.testSystem.domains.InmutableCompositeClass;

@Repository
public interface InmutableCompositeClassRepository extends EntityRepository<InmutableCompositeClass, Long> {
}

package owl.feddericokz.testSystem.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import owl.feddericokz.rest.controllers.EntityRestController;
import owl.feddericokz.testSystem.domains.MutableCompositeClass;
import owl.feddericokz.testSystem.domains.MutableCompositeClassVo;

@RequestMapping("api/resources/mutable-domain")
public interface MutableCompositeClassRestController extends EntityRestController<MutableCompositeClassVo, MutableCompositeClass, Long> {
}

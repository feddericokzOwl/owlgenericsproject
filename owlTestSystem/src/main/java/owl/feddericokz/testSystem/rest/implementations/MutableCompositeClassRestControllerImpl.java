package owl.feddericokz.testSystem.rest.implementations;

import org.springframework.web.bind.annotation.RestController;
import owl.feddericokz.rest.controllers.support.EntityRestControllerSupport;
import owl.feddericokz.testSystem.domains.MutableCompositeClass;
import owl.feddericokz.testSystem.domains.MutableCompositeClassVo;
import owl.feddericokz.testSystem.rest.MutableCompositeClassRestController;

@RestController
public class MutableCompositeClassRestControllerImpl extends EntityRestControllerSupport<MutableCompositeClassVo, MutableCompositeClass, Long>
        implements MutableCompositeClassRestController {
}

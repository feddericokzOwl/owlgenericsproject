package owl.feddericokz.testSystem.services;

import owl.feddericokz.system.services.EntityService;
import owl.feddericokz.testSystem.domains.MutableCompositeClass;

public interface MutableCompositeClassService extends EntityService<MutableCompositeClass,Long> {
}

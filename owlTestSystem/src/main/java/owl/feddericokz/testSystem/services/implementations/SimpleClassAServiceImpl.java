package owl.feddericokz.testSystem.services.implementations;

import org.springframework.stereotype.Service;
import owl.feddericokz.system.domains.EntityVo;
import owl.feddericokz.system.services.support.EntityServiceConfigurer;
import owl.feddericokz.system.services.support.EntityServiceSupport;
import owl.feddericokz.testSystem.domains.SimpleClassA;
import owl.feddericokz.testSystem.domains.SimpleClassAVo;
import owl.feddericokz.testSystem.services.SimpleClassAService;

@Service
public class SimpleClassAServiceImpl extends EntityServiceSupport<SimpleClassA,Long>  implements SimpleClassAService {

    /*
     * Configuration
     */

    @Override
    public void configure(EntityServiceConfigurer configurer) {
        setConfiguration(
                configurer
                        .simpleDomain()
                        .enableDeletes()
                        .enableSaving()
                        .enableQuerys()
                        .configure()
        );
    }

    /*
     * Methods
     */

    @Override
    protected <S extends EntityVo<SimpleClassA, Long>> S getVoInstance() {
        return (S) new SimpleClassAVo();
    }

}

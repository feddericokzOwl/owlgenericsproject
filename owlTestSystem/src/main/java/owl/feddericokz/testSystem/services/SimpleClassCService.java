package owl.feddericokz.testSystem.services;

import owl.feddericokz.system.services.EntityService;
import owl.feddericokz.testSystem.domains.SimpleClassC;

public interface SimpleClassCService extends EntityService<SimpleClassC,Long> {
}

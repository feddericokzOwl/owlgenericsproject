package owl.feddericokz.testSystem.services.implementations;

import org.springframework.stereotype.Service;
import owl.feddericokz.system.domains.EntityVo;
import owl.feddericokz.system.services.support.EntityServiceConfigurer;
import owl.feddericokz.system.services.support.EntityServiceSupport;
import owl.feddericokz.testSystem.domains.SimpleClassB;
import owl.feddericokz.testSystem.domains.SimpleClassBVo;
import owl.feddericokz.testSystem.services.SimpleClassBService;

@Service
public class SimpleClassBServiceImpl extends EntityServiceSupport<SimpleClassB,Long> implements SimpleClassBService {


    /*
     * Configuration
     */

    @Override
    public void configure(EntityServiceConfigurer configurer) {
        setConfiguration(
                configurer
                    .simpleDomain()
                    .enableSaving()
                    .enableQuerys()
                    .enableDeletes()
                    .configure()
        );
    }

    /*
     * Methods
     */

    @Override
    protected <S extends EntityVo<SimpleClassB, Long>> S getVoInstance() {
        return (S) new SimpleClassBVo();
    }

}

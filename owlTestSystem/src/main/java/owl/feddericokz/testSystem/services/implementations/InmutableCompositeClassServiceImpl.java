package owl.feddericokz.testSystem.services.implementations;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import owl.feddericokz.system.domains.EntityVo;
import owl.feddericokz.system.exceptions.EntityNotFoundException;
import owl.feddericokz.system.services.support.EntityServiceConfigurer;
import owl.feddericokz.system.services.support.EntityServiceSupport;
import owl.feddericokz.testSystem.domains.InmutableCompositeClass;
import owl.feddericokz.testSystem.domains.InmutableCompositeClassVo;
import owl.feddericokz.testSystem.domains.InmutableCompositeClass_;
import owl.feddericokz.testSystem.domains.SimpleClassB;
import owl.feddericokz.testSystem.services.InmutableCompositeClassService;
import owl.feddericokz.testSystem.services.SimpleClassBService;

@Service
public class InmutableCompositeClassServiceImpl extends EntityServiceSupport<InmutableCompositeClass,Long> implements InmutableCompositeClassService {

    /*
     * Services
     */

    private SimpleClassBService simpleClassBService;

    /*
     * Constructors
     */

    public InmutableCompositeClassServiceImpl(SimpleClassBService simpleClassBService) {
        this.simpleClassBService = simpleClassBService;
    }

    /*
     * Configuration
     */

    @Override
    public void configure(EntityServiceConfigurer configurer) {
        setConfiguration(
                configurer
                        .compositeDomain()
                            .composedByFetch(SimpleClassB.class)
                                .fromService(simpleClassBService)
                        .and()
                        .inmutableDomain()
                        .enableSaving()
                        .enableQuerys()
                        .enableDeletes()
                        .configure()
        );
    }

    /*
     * Methods
     */

    @Override
    protected <S extends EntityVo<InmutableCompositeClass, Long>> S getVoInstance() {
        return (S) new InmutableCompositeClassVo();
    }

    @Override
    public InmutableCompositeClass _findByT(InmutableCompositeClass entity) throws EntityNotFoundException {
        return this._findOne(((Specification<InmutableCompositeClass>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(InmutableCompositeClass_.SOME_VALUE_INT), entity.getSomeValueInt()))
                .and((Specification<InmutableCompositeClass>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(InmutableCompositeClass_.SOME_VALUE_STRING),entity.getSomeValueString()))
                    .and((Specification<InmutableCompositeClass>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(InmutableCompositeClass_.CLASS_B), entity.getClassB())));
    }

}

package owl.feddericokz.testSystem.services;

import owl.feddericokz.system.services.EntityService;
import owl.feddericokz.testSystem.domains.SimpleClassA;

public interface SimpleClassAService extends EntityService<SimpleClassA,Long> {
}

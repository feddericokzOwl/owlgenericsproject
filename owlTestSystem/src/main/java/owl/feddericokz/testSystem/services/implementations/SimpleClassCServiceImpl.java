package owl.feddericokz.testSystem.services.implementations;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import owl.feddericokz.system.domains.EntityVo;
import owl.feddericokz.system.exceptions.EntityNotFoundException;
import owl.feddericokz.system.services.support.EntityServiceConfigurer;
import owl.feddericokz.system.services.support.EntityServiceSupport;
import owl.feddericokz.testSystem.domains.SimpleClassC;
import owl.feddericokz.testSystem.domains.SimpleClassCVo;
import owl.feddericokz.testSystem.domains.SimpleClassC_;
import owl.feddericokz.testSystem.services.SimpleClassCService;

@Service
public class SimpleClassCServiceImpl extends EntityServiceSupport<SimpleClassC,Long> implements SimpleClassCService {

    /*
     * Configuration
     */

    @Override
    public void configure(EntityServiceConfigurer configurer) {
        setConfiguration(
                configurer
                        .simpleDomain()
                        .findBeforeSaving()
                        .enableSaving()
                        .enableQuerys()
                        .enableDeletes()
                        .configure()
        );
    }

    /*
     * Methods
     */

    @Override
    protected <S extends EntityVo<SimpleClassC, Long>> S getVoInstance() {
        return (S) new SimpleClassCVo();
    }

    @Override
    public SimpleClassC _findByT(SimpleClassC entity) throws EntityNotFoundException {
        return this._findOne((Specification<SimpleClassC>) (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(SimpleClassC_.SOME_VALUE_INT), entity.getSomeValueInt()));
    }

}

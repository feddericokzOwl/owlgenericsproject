package owl.feddericokz.testSystem.services;

import owl.feddericokz.system.services.EntityService;
import owl.feddericokz.testSystem.domains.InmutableCompositeClass;

public interface InmutableCompositeClassService extends EntityService<InmutableCompositeClass,Long> {
}

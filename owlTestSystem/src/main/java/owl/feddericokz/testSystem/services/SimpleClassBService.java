package owl.feddericokz.testSystem.services;

import owl.feddericokz.system.services.EntityService;
import owl.feddericokz.testSystem.domains.SimpleClassB;

public interface SimpleClassBService extends EntityService<SimpleClassB,Long> {

}

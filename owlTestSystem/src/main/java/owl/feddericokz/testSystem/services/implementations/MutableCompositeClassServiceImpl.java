package owl.feddericokz.testSystem.services.implementations;

import org.springframework.stereotype.Service;
import owl.feddericokz.system.domains.EntityVo;
import owl.feddericokz.system.services.support.EntityServiceConfigurer;
import owl.feddericokz.system.services.support.EntityServiceSupport;
import owl.feddericokz.testSystem.domains.InmutableCompositeClass;
import owl.feddericokz.testSystem.domains.MutableCompositeClass;
import owl.feddericokz.testSystem.domains.MutableCompositeClassVo;
import owl.feddericokz.testSystem.domains.SimpleClassA;
import owl.feddericokz.testSystem.services.InmutableCompositeClassService;
import owl.feddericokz.testSystem.services.MutableCompositeClassService;
import owl.feddericokz.testSystem.services.SimpleClassAService;

@Service
public class MutableCompositeClassServiceImpl extends EntityServiceSupport<MutableCompositeClass,Long> implements MutableCompositeClassService {

    /*
     * Services
     */

    private InmutableCompositeClassService inmutableCompositeClassService;
    private SimpleClassAService simpleClassAService;

    /*
     * Constructors
     */

    public MutableCompositeClassServiceImpl(InmutableCompositeClassService inmutableCompositeClassService, SimpleClassAService simpleClassAService) {
        this.inmutableCompositeClassService = inmutableCompositeClassService;
        this.simpleClassAService = simpleClassAService;
    }

    /*
     * Configuration
     */

    @Override
    public void configure(EntityServiceConfigurer configurer) {
        setConfiguration(
                configurer
                        .compositeDomain()
                            .composedBySaveAndUpdate(InmutableCompositeClass.class)
                                .fromService(inmutableCompositeClassService)
                            .composedBySaveAndUpdate(SimpleClassA.class)
                                .fromService(simpleClassAService)
                        .and()
                        .enableSaving()
                        .enableQuerys()
                        .enableDeletes()
                        .configure()
        );
    }

    /*
     * Methods
     */

    @Override
    protected <S extends EntityVo<MutableCompositeClass, Long>> S getVoInstance() {
        return (S) new MutableCompositeClassVo();
    }

}

package owl.feddericokz.testSystem.domains;

import lombok.*;
import owl.feddericokz.system.domains.support.ServiceEnumSupport;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@EqualsAndHashCode(callSuper = true)
@Table(name = "owl_enum_example")
@Data
@Setter(value = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class EnumExample extends ServiceEnumSupport<Long> {

    public static final EnumExample VALUE = new EnumExample(1L,"VALUE");

    /*
     * Fields
     */

    private String denominacion;

    /*
     * Constructors
     */

    public EnumExample(Long id, String denominacion) {
        this.setId(id);
        this.denominacion = denominacion;
    }

    /*
     * Getters - mappings
     */

    @Column(name = "denominacion")
    public String getDenominacion() {
        return denominacion;
    }

}

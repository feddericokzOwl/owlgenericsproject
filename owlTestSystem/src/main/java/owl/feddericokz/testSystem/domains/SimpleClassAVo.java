package owl.feddericokz.testSystem.domains;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import owl.feddericokz.system.domains.ServiceVo;
import owl.feddericokz.system.domains.support.EntityVoSupport;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class SimpleClassAVo extends EntityVoSupport<SimpleClassA,Long> {

    /*
     * Fields
     */

    private String someValueString;
    private Integer someValueInt;
    /*private MutableCompositeClassVo mutableCompositeClass;*/

    /*
     * Constructors
     */

    @Builder
    public SimpleClassAVo(Long id, LocalDateTime versionTag, String someValueString, Integer someValueInt/*, MutableCompositeClassVo mutableCompositeClass*/) {
        super(id, versionTag);
        this.someValueString = someValueString;
        this.someValueInt = someValueInt;
        /*this.mutableCompositeClass = mutableCompositeClass;*/
    }

}

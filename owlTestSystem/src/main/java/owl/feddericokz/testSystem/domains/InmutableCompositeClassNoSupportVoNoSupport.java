package owl.feddericokz.testSystem.domains;

import lombok.Builder;
import lombok.Data;
import owl.feddericokz.system.domains.EntityVo;
import owl.feddericokz.system.domains.ServiceVo;

import java.time.LocalDateTime;

@Data
@Builder
public class InmutableCompositeClassNoSupportVoNoSupport implements EntityVo<InmutableCompositeClassNoSupport, Long> {

    /*
     * Fields
     */

    private Long identifier;
    private LocalDateTime versionTag;
    private String someValueString;
    private Integer someValueInt;
    private SimpleClassBVo classB;

    @Override
    public Long getId() {
        return identifier;
    }

    @Override
    public void setId(Long aLong) {
        this.identifier = aLong;
    }

}

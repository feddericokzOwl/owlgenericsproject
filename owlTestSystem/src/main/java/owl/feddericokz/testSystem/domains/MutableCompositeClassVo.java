package owl.feddericokz.testSystem.domains;

import lombok.*;
import owl.feddericokz.system.domains.ServiceVo;
import owl.feddericokz.system.domains.support.EntityVoSupport;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class MutableCompositeClassVo extends EntityVoSupport<MutableCompositeClass,Long> {

    /*
     * Fields
     */

    private String someValueString;
    private Integer someValueInt;
    private List<SimpleClassAVo> simpleClassAList;
    private InmutableCompositeClassVo inmutableClass;

    /*
     * Constructors
     */

    @Builder
    public MutableCompositeClassVo(Long id, LocalDateTime versionTag, String someValueString, Integer someValueInt, @Singular("simpleClassA") List<SimpleClassAVo> simpleClassAList, InmutableCompositeClassVo inmutableClass) {
        super(id, versionTag);
        this.someValueString = someValueString;
        this.someValueInt = someValueInt;
        this.simpleClassAList = simpleClassAList;
        this.inmutableClass = inmutableClass;
    }

}

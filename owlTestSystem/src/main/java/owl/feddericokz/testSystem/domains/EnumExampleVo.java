package owl.feddericokz.testSystem.domains;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import owl.feddericokz.system.domains.EnumVo;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EnumExampleVo implements EnumVo<EnumExample, Long> {

    /*
     * Fields
     */

    private Long id;
    private String denominacion;

    /*
     * Static methods
     */

    public static final EnumExampleVo VALUE = (EnumExampleVo) new EnumExampleVo().fromDomain(EnumExample.VALUE);

}

package owl.feddericokz.testSystem.domains;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import owl.feddericokz.system.domains.ServiceVo;
import owl.feddericokz.system.domains.support.EntityVoSupport;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class SimpleClassBVo extends EntityVoSupport<SimpleClassB, Long> {

    /*
     * Fields
     */

    private String someValueString;
    private Integer someValueInt;

    /*
     * Constructors
     */

    @Builder
    public SimpleClassBVo(Long id, LocalDateTime versionTag, String someValueString, Integer someValueInt) {
        super(id, versionTag);
        this.someValueString = someValueString;
        this.someValueInt = someValueInt;
    }

}

package owl.feddericokz.testSystem.domains;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import owl.feddericokz.system.domains.support.EntityVoSupport;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
public class InmutableCompositeClassNoSupportVo extends EntityVoSupport<InmutableCompositeClassNoSupport,Long> {

    /*
     * Fields
     */

    private String someValueString;
    private Integer someValueInt;
    private SimpleClassBVo classB;

    /*
     * Constructors
     */

    @Builder
    public InmutableCompositeClassNoSupportVo(Long id, LocalDateTime versionTag, String someValueString, Integer someValueInt, SimpleClassBVo classB) {
        super(id, versionTag);
        this.someValueString = someValueString;
        this.someValueInt = someValueInt;
        this.classB = classB;
    }

}

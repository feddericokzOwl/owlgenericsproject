package owl.feddericokz.testSystem.domains;

import lombok.Setter;
import owl.feddericokz.system.domains.support.ServiceReadySupport;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "mutable_comp_class")
@Setter
public class MutableCompositeClass extends ServiceReadySupport<Long> {

    /*
     * Fields
     */

    private String someValueString;
    private Integer someValueInt;
    private List<SimpleClassA> simpleClassAList;
    private InmutableCompositeClass inmutableClass;

    /*
     * Getters - Mappings
     */

    @Column(name = "some_str_val")
    public String getSomeValueString() {
        return someValueString;
    }

    @Column(name = "some_int_val")
    public Integer getSomeValueInt() {
        return someValueInt;
    }

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "mutable_class_id")
    public List<SimpleClassA> getSimpleClassAList() {
        return simpleClassAList;
    }

    @ManyToOne
    @JoinColumn(name = "inmutable_class_id")
    public InmutableCompositeClass getInmutableClass() {
        return inmutableClass;
    }

    /*
     * Methods
     */

    @Override
    public boolean autoValidate() {
        return inmutableClass!=null;
    }

}

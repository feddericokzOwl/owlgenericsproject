package owl.feddericokz.testSystem.domains;

import lombok.Setter;
import owl.feddericokz.system.domains.ServiceReady;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "inmutable_comp_class_no_support")
@Setter
public class InmutableCompositeClassNoSupport implements ServiceReady<Long> {

    /*
     * Fields
     */

    private Long identifier;
    private String someValueString;
    private Integer someValueInt;
    private SimpleClassB classB;
    private LocalDateTime versionTag;

    /*
     * Getters - Mappings
     */

    @Id
    @Column(name = "identifier")
    public Long getIdentifier() {
        return identifier;
    }

    @Column(name = "some_str_val")
    public String getSomeValueString() {
        return someValueString;
    }

    @Column(name = "some_int_val")
    public Integer getSomeValueInt() {
        return someValueInt;
    }

    @ManyToOne
    @JoinColumn(name = "class_b_id")
    public SimpleClassB getClassB() {
        return classB;
    }



    /*
     * Methods
     */

    @Override
    public boolean autoValidate() {
        return true;
    }

    @Override
    @Transient
    public Long getId() {
        return identifier;
    }

    @Override
    public void setId(Long aLong) {
        this.identifier = aLong;
    }

    @Override
    @Version
    @Column(name = "last_updated")
    public LocalDateTime getVersionTag() {
        return versionTag;
    }

}

package owl.feddericokz.testSystem.domains;

import lombok.Setter;
import owl.feddericokz.system.domains.support.ServiceReadySupport;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "simple_class_a")
@Setter
public class SimpleClassA extends ServiceReadySupport<Long> {

    /*
     * Fields
     */

    private String someValueString;
    private Integer someValueInt;
    /*private MutableCompositeClass mutableCompositeClass;*/

    /*
     * Getters - Mappings
     */

    @Column(name = "some_str_val")
    public String getSomeValueString() {
        return someValueString;
    }

    @Column(name = "some_int_val")
    public Integer getSomeValueInt() {
        return someValueInt;
    }

    /*@ManyToOne
    @JoinColumn(name = "mutable_class_id")
    public MutableCompositeClass getMutableCompositeClass() {
        return mutableCompositeClass;
    }*/

    /*
     * Methods
     */

    @Override
    public boolean autoValidate() {
        return true;
    }

}

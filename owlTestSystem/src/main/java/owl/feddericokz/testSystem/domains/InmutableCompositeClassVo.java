package owl.feddericokz.testSystem.domains;

import lombok.*;
import owl.feddericokz.system.domains.ServiceVo;
import owl.feddericokz.system.domains.support.EntityVoSupport;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InmutableCompositeClassVo extends EntityVoSupport<InmutableCompositeClass,Long> {

    /*
     * Fields
     */

    private String someValueString;
    private Integer someValueInt;
    private SimpleClassBVo classB;

    /*
     * Constructors
     */

    @Builder
    public InmutableCompositeClassVo(Long id, LocalDateTime versionTag, String someValueString, Integer someValueInt, SimpleClassBVo classB) {
        super(id, versionTag);
        this.someValueString = someValueString;
        this.someValueInt = someValueInt;
        this.classB = classB;
    }

}

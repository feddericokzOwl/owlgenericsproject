package owl.feddericokz.testSystem.domains;

import lombok.Builder;
import lombok.Data;
import owl.feddericokz.system.domains.EntityVo;
import owl.feddericokz.system.domains.ServiceVo;

import java.time.LocalDateTime;

@Data
@Builder
public class InmutableCompositeClassVoNoSupport implements EntityVo<InmutableCompositeClass, Long> {

    /*
     * Fields
     */

    private Long id;
    private LocalDateTime versionTag;
    private String someValueString;
    private Integer someValueInt;
    private SimpleClassBVo classB;

}

package owl.feddericokz.testSystem.domains;

import lombok.Setter;
import owl.feddericokz.system.domains.support.ServiceReadySupport;

import javax.persistence.*;

@Entity
@Table(name = "inmutable_comp_class")
@Setter
public class InmutableCompositeClass extends ServiceReadySupport<Long> {

    /*
     * Fields
     */

    private String someValueString;
    private Integer someValueInt;
    private SimpleClassB classB;

    /*
     * Getters - Mappings
     */

    @Column(name = "some_str_val")
    public String getSomeValueString() {
        return someValueString;
    }

    @Column(name = "some_int_val")
    public Integer getSomeValueInt() {
        return someValueInt;
    }

    @ManyToOne
    @JoinColumn(name = "class_b_id")
    public SimpleClassB getClassB() {
        return classB;
    }

    /*
     * Methods
     */

    @Override
    public boolean autoValidate() {
        return true;
    }

}

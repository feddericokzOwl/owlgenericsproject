package owl.feddericokz.testSystem.domains;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import owl.feddericokz.system.domains.ServiceVo;
import owl.feddericokz.system.domains.support.EntityVoSupport;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class SimpleClassCVo extends EntityVoSupport<SimpleClassC, Long> {

    /*
     * Fields
     */

    private String someValueStr;
    private Integer someValueInt;

    /*
     * Constructors
     */

    @Builder
    public SimpleClassCVo(Long id, LocalDateTime versionTag, String someValueStr, Integer someValueInt) {
        super(id, versionTag);
        this.someValueStr = someValueStr;
        this.someValueInt = someValueInt;
    }

}

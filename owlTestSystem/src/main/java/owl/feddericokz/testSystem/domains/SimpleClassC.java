package owl.feddericokz.testSystem.domains;

import lombok.Setter;
import owl.feddericokz.system.domains.support.ServiceReadySupport;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "some_class_c")
@Setter
public class SimpleClassC extends ServiceReadySupport<Long> {

    /*
     * Fields
     */

    private String someValueStr;
    private Integer someValueInt;

    /*
     * Getters - Mappings
     */

    @Column(name = "some_str_val")
    public String getSomeValueStr() {
        return someValueStr;
    }

    @Column(name = "some_int_val")
    public Integer getSomeValueInt() {
        return someValueInt;
    }

    /*
     * Methods
     */

    @Override
    public boolean autoValidate() {
        return someValueInt!=null;
    }

}

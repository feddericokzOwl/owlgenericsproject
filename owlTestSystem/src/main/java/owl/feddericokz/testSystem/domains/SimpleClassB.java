package owl.feddericokz.testSystem.domains;

import lombok.Setter;
import owl.feddericokz.system.domains.support.ServiceReadySupport;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "simple_class_b")
@Setter
public class SimpleClassB  extends ServiceReadySupport<Long> {

    /*
     * Fields
     */

    private String someValueString;
    private Integer someValueInt;

    /*
     * Getters - Mappings
     */

    @Column(name = "some_str_val")
    public String getSomeValueString() {
        return someValueString;
    }

    @Column(name = "some_int_val")
    public Integer getSomeValueInt() {
        return someValueInt;
    }

    /*
     * Methods
     */

    @Override
    public boolean autoValidate() {
        return someValueInt != null && someValueString != null;
    }

}

package owl.feddericokz.testSystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import owl.feddericokz.rest.controllers.owl.configuration.EnableOwlWatch;
import owl.feddericokz.system.logging.configuration.EnableOwlAspectLogging;

@SpringBootApplication
@EnableOwlWatch
@EnableOwlAspectLogging
public class TestAppContextConfiguration {

    public static void main(String[] args) {
        SpringApplication.run(TestAppContextConfiguration.class, args);
    }

}

package owl.feddericokz.system.utils;

import org.junit.Test;
import owl.feddericokz.system.exceptions.InvalidEntityException;
import owl.feddericokz.testSystem.domains.*;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

public class MergingUtilsTest {

    /*
     * Tests
     */

    @Test
    public void fullEntityMergeTest_withoutCollection() throws InvalidEntityException {

        SimpleClassCVo myCVo = SimpleClassCVo.builder()
                .someValueInt(3569)
                .someValueStr("HelloNoob")
                .id(1L)
                .versionTag(LocalDateTime.now())
                .build();

        SimpleClassCVo myCVoUpdate = SimpleClassCVo.builder()
                .someValueInt(798)
                .build();

        SimpleClassC myC = myCVo.toDomain();
        SimpleClassC myCUpdate = myCVoUpdate.toDomain();

        MergingUtils.mergeEntity(myC,myCUpdate);

        assertEquals(myC.getId(),myCVo.getId());
        assertEquals(myC.getVersionTag(), myCVo.getVersionTag());
        assertEquals(myC.getSomeValueInt(), myCVoUpdate.getSomeValueInt());
        assertEquals(myC.getSomeValueStr(), myCVoUpdate.getSomeValueStr());

    }

    @Test
    public void partialEntityMergeTest_withoutCollection() throws InvalidEntityException {

        SimpleClassCVo myCVo = SimpleClassCVo.builder()
                .someValueInt(3569)
                .someValueStr("HelloNoob")
                .id(1L)
                .versionTag(LocalDateTime.now())
                .build();

        SimpleClassCVo myCVoUpdate = SimpleClassCVo.builder()
                .someValueInt(798)
                .build();

        SimpleClassC myC = myCVo.toDomain();
        SimpleClassC myCUpdate = myCVoUpdate.toDomain();

        MergingUtils.mergeEntity(myC,myCUpdate, MergingUtils.PARTIAL);

        assertEquals(myC.getId(),myCVo.getId());
        assertEquals(myC.getVersionTag(), myCVo.getVersionTag());
        assertEquals(myC.getSomeValueInt(), myCVoUpdate.getSomeValueInt());
        assertEquals(myC.getSomeValueStr(), myCVo.getSomeValueStr());

    }

    @Test
    public void fullVoMergeTest_withoutCollection(){

        LocalDateTime versionTag = LocalDateTime.now();

        SimpleClassCVo myCVo = SimpleClassCVo.builder()
                .someValueInt(3569)
                .someValueStr("HelloNoob")
                .id(1L)
                .versionTag(versionTag)
                .build();

        SimpleClassCVo myCVoUpdate = SimpleClassCVo.builder()
                .someValueInt(798)
                .versionTag(LocalDateTime.now())
                .build();

        MergingUtils.mergeVo(myCVo,myCVoUpdate);

        assertEquals(1L, myCVo.getId());
        assertEquals(versionTag, myCVo.getVersionTag());
        assertEquals(798, myCVo.getSomeValueInt());
        assertNull(myCVo.getSomeValueStr());

    }

    @Test
    public void partialVoMergeTest_withoutCollection(){

        LocalDateTime versionTag = LocalDateTime.now();

        SimpleClassCVo myCVo = SimpleClassCVo.builder()
                .someValueInt(3569)
                .someValueStr("HelloNoob")
                .id(1L)
                .versionTag(versionTag)
                .build();

        SimpleClassCVo myCVoUpdate = SimpleClassCVo.builder()
                .someValueInt(798)
                .versionTag(LocalDateTime.now())
                .build();

        MergingUtils.mergeVo(myCVo,myCVoUpdate, MergingUtils.PARTIAL);

        assertEquals(1L, myCVo.getId());
        assertEquals(versionTag, myCVo.getVersionTag());
        assertEquals(798, myCVo.getSomeValueInt());
        assertEquals("HelloNoob", myCVo.getSomeValueStr());

    }

    @Test
    public void fullMergeVoIntoEntity_withoutCollection() throws InvalidEntityException {

        LocalDateTime versionTag = LocalDateTime.now();

        SimpleClassCVo myCVo = SimpleClassCVo.builder()
                .someValueInt(3569)
                .someValueStr("HelloNoob")
                .id(1L)
                .versionTag(versionTag)
                .build();

        SimpleClassC myC = myCVo.toDomain();

        SimpleClassCVo myCVoUpdate = SimpleClassCVo.builder()
                .someValueInt(798)
                .versionTag(LocalDateTime.now())
                .build();

        MergingUtils.mergeVoIntoEntity(myC,myCVoUpdate);

        assertEquals(1L, myC.getId());
        assertEquals(versionTag, myC.getVersionTag());
        assertEquals(798, myC.getSomeValueInt());
        assertNull(myC.getSomeValueStr());

    }

    @Test
    public void partialMergeVoIntoEntity_withoutCollection() throws InvalidEntityException {

        LocalDateTime versionTag = LocalDateTime.now();

        SimpleClassCVo myCVo = SimpleClassCVo.builder()
                .someValueInt(3569)
                .someValueStr("HelloNoob")
                .id(1L)
                .versionTag(versionTag)
                .build();

        SimpleClassC myC = myCVo.toDomain();

        SimpleClassCVo myCVoUpdate = SimpleClassCVo.builder()
                .someValueInt(798)
                .versionTag(LocalDateTime.now())
                .build();

        MergingUtils.mergeVoIntoEntity(myC, myCVoUpdate, MergingUtils.PARTIAL);

        assertEquals(1L, myC.getId());
        assertEquals(versionTag, myC.getVersionTag());
        assertEquals(798, myC.getSomeValueInt());
        assertEquals("HelloNoob", myC.getSomeValueStr());

    }

    @Test
    public void mergeTest_withCollection() throws InvalidEntityException {
        // Set up
        SimpleClassBVo myBVo = SimpleClassBVo.builder()
                .id(1L)
                .versionTag(LocalDateTime.now())
                .someValueInt(3569)
                .someValueString("HelloNoob")
                .build();

        InmutableCompositeClassVo myInmutableVo = InmutableCompositeClassVo.builder()
                .classB(myBVo)
                // Sneaky Throws, Lequi si lees esto alguna vez, yo ya se que el 1L definitivamente existe, lo cargue antes
                //  asique esto jamas puede tirar una excepcion.
                .someValueInt(897)
                .someValueString("ShitNigga")
                .id(1L)
                .versionTag(LocalDateTime.now())
                .build();

        SimpleClassAVo myAVo = SimpleClassAVo.builder()
                .someValueInt(3569)
                .id(1L)
                .versionTag(LocalDateTime.now())
                .someValueString("HelloNoob")
                .build();

        // Succesful valid entity save
        MutableCompositeClassVo myMutableVo = MutableCompositeClassVo.builder()
                .someValueInt(456)
                .someValueString("This is going well")
                .inmutableClass(myInmutableVo)
                .build();

        // Succesful valid entity save
        MutableCompositeClassVo myMutableVoUpdate = MutableCompositeClassVo.builder()
                .someValueInt(879)
                .someValueString("This is going well")
                .simpleClassA(myAVo)
                .inmutableClass(myInmutableVo)
                .build();


        MutableCompositeClass myMutable = myMutableVo.toDomain();
        MutableCompositeClass myMutableUpdate = myMutableVoUpdate.toDomain();

        MergingUtils.mergeEntity(myMutable, myMutableUpdate);

        assertEquals(myMutableUpdate.getSomeValueInt(), myMutable.getSomeValueInt());
        assertEquals(myMutableUpdate.getSimpleClassAList().size(), myMutable.getSimpleClassAList().size());

    }

    @Test
    public void fullMergeVoTest_withCollection() {

        // Set up
        SimpleClassBVo myBVo = SimpleClassBVo.builder()
                .id(1L)
                .versionTag(LocalDateTime.now())
                .someValueInt(3569)
                .someValueString("HelloNoob")
                .build();

        InmutableCompositeClassVo myInmutableVo = InmutableCompositeClassVo.builder()
                .classB(myBVo)
                // Sneaky Throws, Lequi si lees esto alguna vez, yo ya se que el 1L definitivamente existe, lo cargue antes
                //  asique esto jamas puede tirar una excepcion.
                .someValueInt(897)
                .someValueString("ShitNigga")
                .id(1L)
                .versionTag(LocalDateTime.now())
                .build();

        SimpleClassAVo myAVo = SimpleClassAVo.builder()
                .someValueInt(3569)
                .id(1L)
                .versionTag(LocalDateTime.now())
                .someValueString("HelloNoob")
                .build();

        // Succesful valid entity save
        MutableCompositeClassVo myMutableVo = MutableCompositeClassVo.builder()
                .someValueInt(456)
                .someValueString("This is going well")
                .inmutableClass(myInmutableVo)
                .build();

        // Succesful valid entity save
        MutableCompositeClassVo myMutableVoUpdate = MutableCompositeClassVo.builder()
                .someValueInt(879)
                .someValueString("This is going well")
                .simpleClassA(myAVo)
                .inmutableClass(myInmutableVo)
                .build();

        MergingUtils.mergeVo(myMutableVo, myMutableVoUpdate);

        assertEquals(myMutableVoUpdate.getSomeValueInt(), myMutableVo.getSomeValueInt());
        assertEquals(myMutableVoUpdate.getSimpleClassAList().size(), myMutableVo.getSimpleClassAList().size());

    }

    @Test
    public void fullMergeVoIntoEntity_withCollection() throws InvalidEntityException {
        // Set up
        SimpleClassBVo myBVo = SimpleClassBVo.builder()
                .id(1L)
                .versionTag(LocalDateTime.now())
                .someValueInt(3569)
                .someValueString("HelloNoob")
                .build();

        InmutableCompositeClassVo myInmutableVo = InmutableCompositeClassVo.builder()
                .classB(myBVo)
                // Sneaky Throws, Lequi si lees esto alguna vez, yo ya se que el 1L definitivamente existe, lo cargue antes
                //  asique esto jamas puede tirar una excepcion.
                .someValueInt(897)
                .someValueString("ShitNigga")
                .id(1L)
                .versionTag(LocalDateTime.now())
                .build();

        SimpleClassAVo myAVo = SimpleClassAVo.builder()
                .someValueInt(3569)
                .id(1L)
                .versionTag(LocalDateTime.now())
                .someValueString("HelloNoob")
                .build();

        // Succesful valid entity save
        MutableCompositeClassVo myMutableVo = MutableCompositeClassVo.builder()
                .someValueInt(456)
                .someValueString("This is going well")
                .inmutableClass(myInmutableVo)
                .build();

        // Succesful valid entity save
        MutableCompositeClassVo myMutableVoUpdate = MutableCompositeClassVo.builder()
                .someValueInt(879)
                .someValueString("This is going well")
                .simpleClassA(myAVo)
                .inmutableClass(myInmutableVo)
                .build();


        MutableCompositeClass myMutable = myMutableVo.toDomain();
        //MutableCompositeClass myMutableUpdate = myMutableVoUpdate.toDomain();

        MergingUtils.mergeVoIntoEntity(myMutable, myMutableVoUpdate);

        assertEquals(myMutableVoUpdate.getSomeValueInt(), myMutable.getSomeValueInt());
        assertNotNull(myMutable.getSimpleClassAList());
        assertEquals(myMutableVoUpdate.getSimpleClassAList().size(), myMutable.getSimpleClassAList().size());

    }


}

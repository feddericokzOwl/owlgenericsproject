package owl.feddericokz.system.services.support;

import lombok.SneakyThrows;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import owl.feddericokz.system.configurations.TestSystemConfiguration;
import owl.feddericokz.system.exceptions.EmptyResourceException;
import owl.feddericokz.system.exceptions.EntityNotFoundException;
import owl.feddericokz.system.exceptions.InvalidEntityException;
import owl.feddericokz.system.exceptions.StaleResourceException;
import owl.feddericokz.testSystem.TestAppContextConfiguration;
import owl.feddericokz.testSystem.domains.*;
import owl.feddericokz.testSystem.services.InmutableCompositeClassService;
import owl.feddericokz.testSystem.services.MutableCompositeClassService;
import owl.feddericokz.testSystem.services.SimpleClassBService;
import owl.feddericokz.testSystem.services.SimpleClassCService;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Import(TestSystemConfiguration.class)
@SpringBootTest(classes = {
        TestAppContextConfiguration.class
})
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class EntityServiceSupportTest {

    /*
     * Services
     */

    @Autowired
    private SimpleClassCService simpleClassCService;
    @Autowired
    private SimpleClassBService simpleClassBService;
    @Autowired
    private InmutableCompositeClassService inmutableCompositeClassService;
    @Autowired
    private MutableCompositeClassService mutableCompositeClassService;

    /*
     * Tests
     */

    @Test
    @Order(1)
    public void simpleClassService_save() throws InvalidEntityException {
        // Succesful Valid Entity Save
        SimpleClassBVo myBVo = SimpleClassBVo.builder()
                .someValueInt(3569)
                .someValueString("HelloNoob")
                .build();

        SimpleClassBVo serviceResponseVo = null;
        try {
            serviceResponseVo = simpleClassBService.save(myBVo);
            assertEquals(myBVo.getSomeValueInt(),serviceResponseVo.getSomeValueInt());
            assertEquals(myBVo.getSomeValueString(),serviceResponseVo.getSomeValueString());
            assertNotNull(serviceResponseVo.getId());
            assertNotNull(serviceResponseVo.getVersionTag());
        } catch (InvalidEntityException | StaleResourceException e) {
            fail();
            // This should not happen here.
        }

        // Invalid Entity Save
        SimpleClassB simpleClassB = myBVo.toDomain();
        try {
            Field field = simpleClassB.getClass().getDeclaredField("someValueInt");
            field.setAccessible(true);
            field.set(simpleClassB,null);
            SimpleClassB finalSimpleClassB = simpleClassB;
            assertThrows(InvalidEntityException.class,() -> simpleClassBService._save(finalSimpleClassB));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            fail();
            // This should not happen.
        }

        // Stale Resource Entity Save
        try {
            myBVo = simpleClassBService.findById(1L);
            myBVo.setSomeValueInt(5968);
            // Save this for later.

            // Update entity so previus copy is stale.
            simpleClassB = simpleClassBService._findById(1L);
            simpleClassB.setSomeValueInt(7777);
            simpleClassBService._save(simpleClassB);

            // Try to save the first copy
            SimpleClassBVo finalMyBVo = myBVo;
            assertThrows(StaleResourceException.class,() -> simpleClassBService.save(finalMyBVo));
        } catch (StaleResourceException | InvalidEntityException | EntityNotFoundException e) {
            fail();
            // This should not happen.
        }
    }

    @Test
    @Order(2)
    public void entityService_findById() {
        // Succesful findById
        try {
            SimpleClassBVo serviceResponseVo = simpleClassBService.findById(1L);
            assertEquals(7777,serviceResponseVo.getSomeValueInt());
            assertEquals("HelloNoob",serviceResponseVo.getSomeValueString());
        } catch (EntityNotFoundException e) {
            fail();
            // This should not happen here.
        }

        // Failed findById
        assertThrows(EntityNotFoundException.class, () -> simpleClassBService.findById(2L));
    }

    @Test
    @Order(3)
    @SneakyThrows
    public void inmutableCompositeClass_save() {
        // Succcesful valid entity save
        InmutableCompositeClassVo myInmutableVo = InmutableCompositeClassVo.builder()
                .classB(simpleClassBService.findById(1L))
                // Sneaky Throws, Lequi si lees esto alguna vez, yo ya se que el 1L definitivamente existe, lo cargue antes
                //  asique esto jamas puede tirar una excepcion.
                .someValueInt(897)
                .someValueString("ShitNigga")
                .build();

        InmutableCompositeClassVo serviceResponseVo = null;

        try {
            serviceResponseVo = inmutableCompositeClassService.save(myInmutableVo);
            assertNotNull(serviceResponseVo.getId());
            assertNotNull(serviceResponseVo.getVersionTag());
            assertEquals(serviceResponseVo.getClassB().getId(), 1L);
        } catch (InvalidEntityException | StaleResourceException e) {
            fail();
            e.printStackTrace();
        }

        InmutableCompositeClassVo serviceResponseVoRetry = null;

        // As this is inmutable, same values should return same Id;

        try {
            serviceResponseVoRetry = inmutableCompositeClassService.save(myInmutableVo);
            assertEquals(serviceResponseVoRetry.getId(), serviceResponseVo.getId());
        } catch (InvalidEntityException | StaleResourceException e) {
            fail();
            e.printStackTrace();
        }

    }

    @Test
    @Order(4)
    @SneakyThrows
    public void mutableCompositeClass_save() {
        // Set up
        InmutableCompositeClassVo myInmutableVo = InmutableCompositeClassVo.builder()
                .classB(simpleClassBService.findById(1L))
                // Sneaky Throws, Lequi si lees esto alguna vez, yo ya se que el 1L definitivamente existe, lo cargue antes
                //  asique esto jamas puede tirar una excepcion.
                .someValueInt(897)
                .someValueString("ShitNigga")
                .build();

        SimpleClassAVo myAVo = SimpleClassAVo.builder()
                .someValueInt(3569)
                .someValueString("HelloNoob")
                .build();

        // Succesful valid entity save
        MutableCompositeClassVo myMutableVo = MutableCompositeClassVo.builder()
                .someValueInt(456)
                .someValueString("This is going well")
                .simpleClassA(myAVo)
                .inmutableClass(myInmutableVo)
                .build();

        MutableCompositeClassVo serviceResponse = null;

        try {
            serviceResponse = mutableCompositeClassService.save(myMutableVo);
            assertNotNull(serviceResponse.getId());
            assertNotNull(serviceResponse.getSimpleClassAList().get(0).getId());
            assertEquals(serviceResponse.getInmutableClass().getId(), 1L); // I can assert this cause i know its going to be 1.
        } catch (InvalidEntityException | StaleResourceException e) {
            fail();
            e.printStackTrace();
        }

        MutableCompositeClassVo serviceResponseRetry = null;

        try {
            serviceResponseRetry = mutableCompositeClassService.save(myMutableVo);
            assertNotEquals(serviceResponse.getId(), serviceResponseRetry.getId());
        } catch (StaleResourceException | InvalidEntityException e) {
            fail();
            e.printStackTrace();
        }

    }

    @Test
    @Order(5)
    @SneakyThrows
    public void findBeforeSave_test() {
        SimpleClassCVo myCVo = SimpleClassCVo.builder()
                .someValueInt(3569)
                .someValueStr("HelloNoob")
                .build();

        try {
            simpleClassCService.save(myCVo);
        } catch (InvalidEntityException | StaleResourceException e) {
            fail();
            e.printStackTrace();
        }

        SimpleClassCVo fetchedEntity = simpleClassCService.findById(1L); // Sneaky Throws.

        assertEquals(fetchedEntity.getSomeValueInt(),3569);
        assertEquals(fetchedEntity.getSomeValueStr(), "HelloNoob");

        myCVo.setSomeValueStr("This should change.");

        try {
            simpleClassCService.save(myCVo);
        } catch (InvalidEntityException | StaleResourceException e) {
            fail();
            e.printStackTrace();
        }

        SimpleClassCVo fetchedEntityRetry = simpleClassCService.findById(1L); // Sneaky Throws.

        assertEquals(3569, fetchedEntityRetry.getSomeValueInt());
        assertEquals("This should change.", fetchedEntityRetry.getSomeValueStr());

    }

    @Test
    @Order(6)
    @SneakyThrows
    public void simpleClass_update() {

        SimpleClassBVo myBVo = simpleClassBService.findById(1L); // Sneaky Throws

        myBVo.setSomeValueInt(987);
        myBVo.setSomeValueString("Updated.");

        try {
            SimpleClassBVo serviceResponse = simpleClassBService.save(myBVo);
            assertEquals(serviceResponse.getSomeValueString(), myBVo.getSomeValueString());
            assertEquals(serviceResponse.getSomeValueInt(), myBVo.getSomeValueInt());
            assertNotEquals(serviceResponse.getVersionTag(),myBVo.getVersionTag());
        } catch (InvalidEntityException | StaleResourceException e) {
            fail();
            e.printStackTrace();
        }

    }

    @Test
    @Order(7)
    @SneakyThrows
    public void mutableCompositeClass_update() {

        MutableCompositeClassVo myMutableClass = mutableCompositeClassService.findById(1L);

        myMutableClass.setSomeValueInt(777);

        try {
            MutableCompositeClassVo serviceResponse = mutableCompositeClassService.save(myMutableClass);
            assertNotEquals(myMutableClass.getVersionTag(), serviceResponse.getVersionTag());
            assertEquals(myMutableClass.getSimpleClassAList().get(0), serviceResponse.getSimpleClassAList().get(0));
        } catch (InvalidEntityException | StaleResourceException e) {
            fail();
            e.printStackTrace();
        }

    }

    @Test
    @Order(8)
    @SneakyThrows
    public void saveAll_newEntities_test() {
            // Obtengo una lista con todos los objetos que hay hasta el momento.
            List<SimpleClassCVo> originalList = simpleClassCService.findAll(); // Sneaky Throws.

            // Creo los nuevos objetos que voy a guardar.
            SimpleClassCVo simpleClassCVo1 = SimpleClassCVo.builder()
                    .someValueInt(555)
                    .someValueStr("sss")
                    .build();

            SimpleClassCVo simpleClassCVo2 = SimpleClassCVo.builder()
                    .someValueInt(663)
                    .someValueStr("sssk")
                    .build();

            List<SimpleClassCVo> newEntitiesList = Arrays.asList(simpleClassCVo1,simpleClassCVo2);

            try {
                List<SimpleClassCVo> serviceResponse = simpleClassCService.saveAll(newEntitiesList);
                for (SimpleClassCVo simpleClassCVo : serviceResponse) {
                    assertNotNull(simpleClassCVo.getId());
                }
                assertEquals(originalList.size()+newEntitiesList.size(), simpleClassCService.findAll().size());

            } catch (StaleResourceException | InvalidEntityException e) {
                e.printStackTrace();
                fail();
            }
    }

    @Test
    @Order(9)
    @SneakyThrows
    public void transactional_saveAll_update_staleObject() {

            // Haciendo bulto.
            SimpleClassBVo myBVo = SimpleClassBVo.builder()
                .someValueInt(3569)
                .someValueString("HelloNoob")
                .build();

            simpleClassBService.save(myBVo);

            // Obtengo una lista con todos los objetos que hay hasta el momento.
            List<SimpleClassBVo> originalList = simpleClassBService.findAll(); // Sneaky throws.

            // Make some changes to the list
            originalList.get(0).setSomeValueString("Changed this.");
            originalList.get(1).setSomeValueString("Changed this too.");

            // Make changes in database before saving the list.
            SimpleClassBVo modified = simpleClassBService.findById(originalList.get(1).getId()); // Sneaky throws.
            modified.setSomeValueString("Got here first.");
            modified = simpleClassBService.save(modified); // Sneaky throws.

            assertNotEquals(originalList.get(1).getVersionTag(), modified.getVersionTag());

            // Save the original list with it's changes and watch it explode.
            try {
                List<SimpleClassBVo> serviceResponse = simpleClassBService.saveAll(originalList);
                fail();
            } catch (StaleResourceException | InvalidEntityException e) {
                e.printStackTrace();
                assertNotEquals("Changed this.", ((SimpleClassBVo) simpleClassBService.findById(originalList.get(0).getId())).getSomeValueString());
            }
    }

    @Test
    @Order(10)
    public void transactional_saveAll_newEntities_invalidEntity() {
        try {
            // Obtengo una lista con todos los objetos que hay hasta el momento.
            List<SimpleClassCVo> originalList = simpleClassCService.findAll();

            // Creo los nuevos objetos que voy a guardar.
            SimpleClassCVo simpleClassCVo1 = SimpleClassCVo.builder()
                    .someValueInt(165)
                    .someValueStr("das")
                    .build();

            SimpleClassCVo simpleClassCVo2 = SimpleClassCVo.builder()
                    .someValueInt(654)
                    .someValueStr("uio")
                    .build();

            // To test this i need to make one of the domains invalid.
            SimpleClassC simpleClassCBroken = simpleClassCVo2.toDomain();
            simpleClassCBroken.setSomeValueInt(null);

            List<SimpleClassC> newEntitiesList = Arrays.asList(simpleClassCVo1.toDomain(),simpleClassCBroken);

            try {
                List<SimpleClassC> serviceResponse = simpleClassCService._saveAll(newEntitiesList);
                fail();
            } catch (StaleResourceException | InvalidEntityException e) {
                e.printStackTrace();
                assertEquals(originalList.size(), simpleClassCService.findAll().size());
            }

        } catch (EmptyResourceException /*| InvalidEntityException*/ e) {
            fail();
            e.printStackTrace();
        }
    }

}
package owl.feddericokz.system.domains.support;

import org.junit.jupiter.api.Test;
import owl.feddericokz.system.exceptions.InvalidEntityException;
import owl.feddericokz.testSystem.domains.*;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EntityVoTest {

    @Test
    public void toDomainTest_support_class_to_support_class() throws InvalidEntityException {
        SimpleClassBVo myBVo = SimpleClassBVo.builder()
                .someValueInt(3569)
                .someValueString("HelloNoob")
                .id(1L)
                .versionTag(LocalDateTime.now())
                .build();

        InmutableCompositeClassVo myInmutableVo = InmutableCompositeClassVo.builder()
                .id(35L)
                .classB(myBVo)
                .someValueInt(897)
                .someValueString("ShitNigga")
                .versionTag(LocalDateTime.now())
                .build();

        InmutableCompositeClass myInmutableClass = myInmutableVo.toDomain();

        assertEquals(myInmutableClass.getId(),myInmutableVo.getId());
        assertEquals(myInmutableClass.getSomeValueInt(),myInmutableVo.getSomeValueInt());
        assertEquals(myInmutableClass.getSomeValueString(),myInmutableVo.getSomeValueString());
        assertEquals(myInmutableClass.getVersionTag(),myInmutableVo.getVersionTag());

        assertEquals(myInmutableClass.getClassB().getId(),myBVo.getId());
        assertEquals(myInmutableClass.getClassB().getSomeValueInt(),myBVo.getSomeValueInt());
        assertEquals(myInmutableClass.getClassB().getSomeValueString(),myBVo.getSomeValueString());
        assertEquals(myInmutableClass.getClassB().getVersionTag(),myBVo.getVersionTag());
    }

    @Test
    public void toDomainTest_no_support_class_to_support_class() throws InvalidEntityException {
        SimpleClassBVo myBVo = SimpleClassBVo.builder()
                .someValueInt(3569)
                .someValueString("HelloNoob")
                .id(1L)
                .versionTag(LocalDateTime.now())
                .build();

        InmutableCompositeClassVoNoSupport myInmutableVo = InmutableCompositeClassVoNoSupport.builder()
                .id(37L)
                .someValueInt(465)
                .someValueString("CoronaShit")
                .versionTag(LocalDateTime.now())
                .classB(myBVo)
                .build();

        InmutableCompositeClass myInmutableClass = myInmutableVo.toDomain();

        assertEquals(myInmutableClass.getId(),myInmutableVo.getId());
        assertEquals(myInmutableClass.getSomeValueInt(),myInmutableVo.getSomeValueInt());
        assertEquals(myInmutableClass.getSomeValueString(),myInmutableVo.getSomeValueString());
        assertEquals(myInmutableClass.getVersionTag(),myInmutableVo.getVersionTag());

        assertEquals(myInmutableClass.getClassB().getId(),myBVo.getId());
        assertEquals(myInmutableClass.getClassB().getSomeValueInt(),myBVo.getSomeValueInt());
        assertEquals(myInmutableClass.getClassB().getSomeValueString(),myBVo.getSomeValueString());
        assertEquals(myInmutableClass.getClassB().getVersionTag(),myBVo.getVersionTag());
    }

    @Test
    public void toDomainTest_support_class_to_no_support_class() throws InvalidEntityException {
        SimpleClassBVo myBVo = SimpleClassBVo.builder()
                .someValueInt(3569)
                .someValueString("HelloNoob")
                .id(1L)
                .versionTag(LocalDateTime.now())
                .build();

        InmutableCompositeClassNoSupportVo myInmutableVo = InmutableCompositeClassNoSupportVo.builder()
                .id(35L)
                .classB(myBVo)
                .someValueInt(897)
                .someValueString("ShitNigga")
                .versionTag(LocalDateTime.now())
                .build();

        InmutableCompositeClassNoSupport myInmutableClass = myInmutableVo.toDomain();

        assertEquals(myInmutableClass.getId(),myInmutableVo.getId());
        assertEquals(myInmutableClass.getSomeValueInt(),myInmutableVo.getSomeValueInt());
        assertEquals(myInmutableClass.getSomeValueString(),myInmutableVo.getSomeValueString());
        assertEquals(myInmutableClass.getVersionTag(),myInmutableVo.getVersionTag());

        assertEquals(myInmutableClass.getClassB().getId(),myBVo.getId());
        assertEquals(myInmutableClass.getClassB().getSomeValueInt(),myBVo.getSomeValueInt());
        assertEquals(myInmutableClass.getClassB().getSomeValueString(),myBVo.getSomeValueString());
        assertEquals(myInmutableClass.getClassB().getVersionTag(),myBVo.getVersionTag());
    }

    @Test
    public void toDomainTest_no_support_class_to_no_support_class() throws InvalidEntityException {
        SimpleClassBVo myBVo = SimpleClassBVo.builder()
                .someValueInt(3569)
                .someValueString("HelloNoob")
                .id(1L)
                .versionTag(LocalDateTime.now())
                .build();

        InmutableCompositeClassNoSupportVoNoSupport myInmutableVo = InmutableCompositeClassNoSupportVoNoSupport.builder()
                .identifier(35L)
                .classB(myBVo)
                .someValueInt(897)
                .someValueString("ShitNigga")
                .versionTag(LocalDateTime.now())
                .build();

        InmutableCompositeClassNoSupport myInmutableClass = myInmutableVo.toDomain();

        assertEquals(myInmutableClass.getId(),myInmutableVo.getId());
        assertEquals(myInmutableClass.getSomeValueInt(),myInmutableVo.getSomeValueInt());
        assertEquals(myInmutableClass.getSomeValueString(),myInmutableVo.getSomeValueString());
        assertEquals(myInmutableClass.getVersionTag(),myInmutableVo.getVersionTag());

        assertEquals(myInmutableClass.getClassB().getId(),myBVo.getId());
        assertEquals(myInmutableClass.getClassB().getSomeValueInt(),myBVo.getSomeValueInt());
        assertEquals(myInmutableClass.getClassB().getSomeValueString(),myBVo.getSomeValueString());
        assertEquals(myInmutableClass.getClassB().getVersionTag(),myBVo.getVersionTag());
    }

    // todo Implement test to test collections conversion.

}
package owl.feddericokz.system.domains.support;

import org.junit.jupiter.api.Test;
import owl.feddericokz.testSystem.domains.EnumExample;
import owl.feddericokz.testSystem.domains.EnumExampleVo;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

public class EnumVoTest {

    @Test
    public void fromDomain_test() {

        EnumExampleVo value = EnumExampleVo.VALUE;

        assertThat(value.getId(), is(notNullValue()));
        assertThat(value.getDenominacion(),is("VALUE"));

    }

    @Test
    public void toDomain_test() {

        EnumExample value = EnumExampleVo.VALUE.toDomain();

        assertThat(value.getId(), is(notNullValue()));
        assertThat(value.getDenominacion(), is("VALUE"));

    }

}

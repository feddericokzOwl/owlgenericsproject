package owl.feddericokz.system.configurations;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@TestConfiguration
@ComponentScan(basePackages = {
        "owl.feddericokz.testSystem.services.implementations"
})
@EntityScan(basePackages = {
        "owl.feddericokz.testSystem.domains"
})
@EnableJpaRepositories(basePackages = {
        "owl.feddericokz.testSystem.repositories"
})
public class TestSystemConfiguration {
}

package owl.feddericokz.system.services.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import owl.feddericokz.system.services.PermissionsService;
import owl.feddericokz.system.services.support.PermissionsServiceSupport;

@Configuration
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true,
        jsr250Enabled = true)
public class OwlSecurityConfiguration {

    /*
     * Beans
     */

    @Bean
    @ConditionalOnMissingBean
    public PermissionsService permissionsService() {
        return new PermissionsServiceSupport();
    }

}

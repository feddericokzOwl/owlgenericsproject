package owl.feddericokz.system.services;

import org.springframework.security.core.Authentication;

/**
 * Defines the contract for a Service that answers if a certain user
 * can execute a given method on a give service.
 * It is intended to be used with SpringSecurity.
 */
public interface PermissionsService {

    /**
     * Asks if a user can execute methods on a given service.
     * @param authentication SpringSecurity Authentication object. This can be obtained from the security context.
     * @param service Name of the service.
     * @return Returns true if the user has permission, false otherwise
     */
    boolean hasPermission(Authentication authentication, String service);

    /**
     * Asks if a user can execute a given method on a given service.
     * @param authentication SpringSecurity Authentication object. This can be obtained from the security context.
     * @param service Name of the service.
     * @param method Name of the method.
     * @return Returns true if the user has permission, false otherwise
     */
    boolean hasPermission(Authentication authentication, String service, String method);

}

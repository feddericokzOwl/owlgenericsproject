package owl.feddericokz.system.services.support;

import lombok.Setter;
import lombok.Singular;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import owl.feddericokz.system.exceptions.InvalidEntityException;
import owl.feddericokz.system.exceptions.StaleResourceException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * So whats the deal with this class..
 * Turns out, @Transactional annotation, sometimes (for example when the optimistic lock check fails) marks transactions as rollback only
 * and since this works with AOP, i cant really catch the UnexpectedRoLLBackException, i dont remember it's full name,
 * So this wrapper catches exceptions thrown from the transaction framework, and checks if it should translate them or not.
 *
 * For a more concrete case, when the StaleResourceException is thrown from inside a transaction, it already has been marked as rollback only
 * so the transaction framework will swallow all of this and throw an UnexpectedRollBackException to the rest of the code, i dont want this behavior,
 * so this class catches that, check if it should have been an StaleResourceException and throws that back to the rest of the code.
 * The same happens with InvalidEntityException.
 */
@Slf4j
@Component
public class EntityServiceTransactionWrapper {

    /*
     * Constants
     */

    public static final String STALE_EXCEPTION = "STALE_EXCEPTION";
    public static final String INVALID_EXCEPTION = "INVALID_EXCEPTIOn";

    /*
     * Services
     */

    @Autowired
    private ApplicationContext applicationContext;

    /*
     * Fields
     */

    @Setter
    private Boolean staleException;
    @Setter
    private Boolean invalidException;

    @Setter
    private Map<String,Throwable> exceptionsMap = new HashMap<>();

    /*
     * Setter
     */

    public void putInExceptionsMap(String key, Throwable value) {
        exceptionsMap.put(key,value);
    }

    /*
     * Methods
     */

    public Object wrap(Class beanClass, String methodName, Class parameterClass, Object parameter) throws Throwable {
        staleException = false;
        invalidException = false;
        EntityServiceSupport entityServiceSupport = (EntityServiceSupport) applicationContext.getBean(beanClass);
        try {
            Method method = entityServiceSupport.getClass().getDeclaredMethod(methodName, parameterClass);
            method.setAccessible(true);
            try {
                return method.invoke(entityServiceSupport, parameter);
            } catch (IllegalAccessException | InvocationTargetException e) {

                log.error("Error in method invokation - beanName: " + beanClass.getSimpleName() +  " - methodName: "+ methodName,e);

                if (e.getCause() instanceof InvalidEntityException) throw (InvalidEntityException) e.getCause();

                if (staleException) {
                    if (exceptionsMap.containsKey(STALE_EXCEPTION)) throw exceptionsMap.get(STALE_EXCEPTION);
                    else throw new StaleResourceException();
                }
                if (invalidException) {
                    if (exceptionsMap.containsKey(INVALID_EXCEPTION)) throw exceptionsMap.get(INVALID_EXCEPTION);
                    else throw new InvalidEntityException(parameterClass);
                }

                throw new RuntimeException(e);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Method not supported.");
        }
    }

}

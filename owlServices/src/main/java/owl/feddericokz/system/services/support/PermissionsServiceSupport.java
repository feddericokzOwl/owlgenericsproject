package owl.feddericokz.system.services.support;

import org.springframework.security.core.Authentication;
import owl.feddericokz.system.services.PermissionsService;

public class PermissionsServiceSupport implements PermissionsService {

    /*
     * Methods
     */

    @Override
    public boolean hasPermission(Authentication authentication, String service) {
        return hasPermission(authentication,service,null);
    }

    @Override
    public boolean hasPermission(Authentication authentication, String service, String method) {
        return true;
    }

}

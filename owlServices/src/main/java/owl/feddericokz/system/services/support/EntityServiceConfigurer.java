package owl.feddericokz.system.services.support;

import owl.feddericokz.system.domains.ServiceReady;
import owl.feddericokz.system.services.EntityService;

import java.util.HashMap;
import java.util.Map;

public class EntityServiceConfigurer {

    /*
     * Fields
     */

    //private EntityServiceConfiguration configuration;
    private boolean isComposite = false;
    private boolean isMutable = true;
    private boolean shouldFindBeforeSave = false;
    private boolean savesEnabled = false;
    private boolean querysEnabled = true;
    private boolean deletesEnabled = false;
    private Map<Class<? extends ServiceReady>, EntityService> compositeObjetsMap = new HashMap<>();
    private Map<Class<? extends ServiceReady>, Boolean> compositeBooleansMap = new HashMap<>();

    /*
     * Methods
     */

    public EntityServiceCompositeDomainConfigurer compositeDomain() {
        isComposite = true;
        return new EntityServiceCompositeDomainConfigurer(this);
    }

    public EntityServiceConfigurer simpleDomain() {
        isComposite = false;
        return this;
    }

    public EntityServiceConfigurer findBeforeSaving() {
        this.shouldFindBeforeSave = true;
        return this;
    }

    public EntityServiceConfigurer inmutableDomain() {
        this.isMutable = false;
        return this;
    }

    public EntityServiceConfigurer enableSaving() {
        this.savesEnabled = true;
        return this;
    }

    public EntityServiceConfigurer disableSaving() {
        this.savesEnabled = false;
        return this;
    }

    public EntityServiceConfigurer enableQuerys() {
        this.querysEnabled = true;
        return this;
    }

    public EntityServiceConfigurer disableQuerys() {
        this.querysEnabled = false;
        return this;
    }

    public EntityServiceConfigurer enableDeletes() {
        this.deletesEnabled = true;
        return this;
    }

    public EntityServiceConfigurer disableDeletes() {
        this.deletesEnabled = false;
        return this;
    }

    public EntityServiceConfiguration configure() {
        return new EntityServiceConfiguration(
                isComposite,
                isMutable,
                shouldFindBeforeSave,
                compositeObjetsMap,
                compositeBooleansMap,
                savesEnabled,
                querysEnabled,
                deletesEnabled
        );
    }

    /*
     * Classes
     */

    public class EntityServiceCompositeDomainConfigurer {

        /*
         * Fields
         */

        private EntityServiceConfigurer configurer;

        /*
         * Constructors
         */

        public EntityServiceCompositeDomainConfigurer(EntityServiceConfigurer configurer) {
            this.configurer = configurer;
        }

        /*
         * Methods
         */

        public ComposedByConfigurer composedByFetch(Class<? extends ServiceReady> clazz) {
            return new ComposedByConfigurer(clazz,false,this);
        }

        public ComposedByConfigurer composedBySaveAndUpdate(Class<? extends ServiceReady> clazz) {
            return new ComposedByConfigurer(clazz,true,this);
        }

        private void put(Class<? extends ServiceReady> clazz, boolean isComposite,EntityService entityService){
            compositeObjetsMap.put(clazz,entityService);
            compositeBooleansMap.put(clazz,isComposite);
        }

        public EntityServiceConfigurer and() {
            return configurer;
        }

        public class ComposedByConfigurer {

            /*
             * Fields
             */

            private Class<? extends ServiceReady> clazz;
            private boolean isComposite;
            private EntityServiceCompositeDomainConfigurer configurer;

            /*
             * Constructors
             */

            public ComposedByConfigurer(Class<? extends ServiceReady> clazz, boolean isComposite, EntityServiceCompositeDomainConfigurer configurer) {
                this.configurer = configurer;
                this.isComposite = isComposite;
                this.clazz = clazz;
            }

            /*
             * Methods
             */

            public EntityServiceCompositeDomainConfigurer fromService(EntityService service) {
                configurer.put(clazz,isComposite,service);
                return configurer;
            }

        }

    }



}

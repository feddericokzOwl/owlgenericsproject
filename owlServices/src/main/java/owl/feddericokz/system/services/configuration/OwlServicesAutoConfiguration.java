package owl.feddericokz.system.services.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {
        "owl.feddericokz.system.services.support"
})
public class OwlServicesAutoConfiguration {
}

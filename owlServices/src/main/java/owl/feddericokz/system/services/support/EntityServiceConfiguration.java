package owl.feddericokz.system.services.support;

import lombok.AllArgsConstructor;
import owl.feddericokz.system.domains.ServiceReady;
import owl.feddericokz.system.exceptions.NotConfiguredException;
import owl.feddericokz.system.services.EntityService;

import java.util.Map;

@AllArgsConstructor
public class EntityServiceConfiguration {

    /*
     * Fields
     */

    private Boolean composite;
    private Boolean mutable;
    private Boolean findBeforeSave;
    private Map<Class<? extends ServiceReady>, EntityService> compositeObjetsMap;
    private Map<Class<? extends ServiceReady>, Boolean> compositeBooleansMap;
    private Boolean savesEnabled;
    private Boolean querysEnabled;
    private Boolean deletesEnabled;


    /*
     * Methods
     */

    /**
     * Determina si el dominio esta compuesto por otros objetos del dominio.
     * @return Retorta true si lo esta, falso en caso contrario.
     */
    public Boolean isComposite() {
        if (composite==null) throw new NotConfiguredException();
        return composite;
    }

    /**
     * Determina si cierta clase forma parte de los objetos de
     * dominio que componen el dominio en cuestion.
     * @param clazz
     * @return Retorna true si la clase es componente, falso en caso contrario.
     */
    public boolean contains(Class<?> clazz) {
        return compositeObjetsMap.containsKey(clazz);
    }

    /**
     * Busca el servicio asociado a la clase componente.
     * @param clazz
     * @return Retorna el servicio asociado a la clase componente, nulo en caso de no haber uno.
     */
    public EntityService getService(Class<?> clazz) {
        return compositeObjetsMap.get(clazz);
    }

    /**
     * Determina si el objeto componente tambien es un objeto compuesto.
     * @param clazz
     * @return Retorna true en caso de serlo, falso en caso contrario.
     */
    public boolean alsoComposite(Class<?> clazz) {
        return compositeBooleansMap.get(clazz);
    }

    /**
     * Determina si el objeto de dominio es mutable o inmutable.
     * @return Retorna true en caso de ser un objeto mutable, falso en caso
     * de ser inmutable.
     */
    public boolean isMutable() {
        if (mutable==null) throw new NotConfiguredException();
        return mutable;
    }

    /**
     * Determina si el objeto de dominio deberia ser buscado por sus caracteristicas
     * antes de ser creado.
     * @return Retorna true si deberia ser buscado, false en caso contrario.
     */
    public boolean shouldFindBeforeSave() {
        if (findBeforeSave==null) throw new NotConfiguredException();
        return findBeforeSave;
    }

    /**
     * Determina si este servicio tiene permitido crear nuevos objetos.
     * @return Retorna true en caso de poder, falso en caso contrario.
     */
    public boolean areSavesAllowed() {
        if (savesEnabled ==null) throw new NotConfiguredException();
        return savesEnabled;
    }

    /**
     * Determina si este servicio tiene permitido hacer consultas.
     * @return Retorna true en caso de poder, falso en caso contrario.
     */
    public boolean areQuerysAllowed() {
        if (querysEnabled ==null) throw new NotConfiguredException();
        return querysEnabled;
    }

    /**
     * Determina si este servicio tiene permitido borrar objetos.
     * @return Retorna true en caso de poder, falso en caso contrario.
     */
    public boolean areDeletesAllowed() {
        if (deletesEnabled ==null) throw new NotConfiguredException();
        return deletesEnabled;
    }

}
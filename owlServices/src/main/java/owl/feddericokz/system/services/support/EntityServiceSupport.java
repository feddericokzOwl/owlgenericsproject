package owl.feddericokz.system.services.support;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.util.Assert;
import owl.feddericokz.system.domains.EntityVo;
import owl.feddericokz.system.domains.ServiceReady;
import owl.feddericokz.system.domains.ServiceVo;
import owl.feddericokz.system.domains.support.OwlCollectionsFactory;
import owl.feddericokz.system.domains.support.ServiceReadySupport;
import owl.feddericokz.system.exceptions.*;
import owl.feddericokz.system.repositories.EntityRepository;
import owl.feddericokz.system.services.EntityService;
import owl.feddericokz.system.utils.MergingUtils;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.*;
import java.util.*;
import java.util.stream.Collectors;

import static owl.feddericokz.system.utils.OwlReflectionUtils.implementsInterface;
import static owl.feddericokz.system.utils.OwlReflectionUtils.subclassOf;

@Slf4j
public abstract class EntityServiceSupport<T extends ServiceReady<ID>, ID extends Serializable> implements EntityService<T,ID> {

    /*
     * Fields
     */

    private EntityServiceConfiguration configuration;

    @Autowired
    private EntityServiceTransactionWrapper entityServiceTransactionWrapper;

    /*
     * Services
     */

    @Autowired
    private EntityRepository<T, ID> repository;
    @PersistenceContext
    private EntityManager entityManager;

    /*
     * Constructor
     */

    public EntityServiceSupport(EntityRepository<T, ID> repository) {
        this.repository = repository;
    }

    public EntityServiceSupport() {
    }

    /*
     * Init
     */

    @PostConstruct
    public void setUp() {
        configure(new EntityServiceConfigurer());
    }

    /*
     * Configuration
     */

    public void configure(EntityServiceConfigurer configurer) {
        setConfiguration(
                configurer
                        .simpleDomain()
                        .disableDeletes()
                        .disableSaving()
                        .enableQuerys()
                        .configure()
        );
    }

    protected final void setConfiguration(EntityServiceConfiguration configuration) {
        this.configuration = configuration;
    }

    /*
     * EntityService Methods
     */

    @Override
    public Long countAll() {
        return repository.count();
    }

    @Override
    public void delete(T entity) {
        shootIfDeletesDisabled();
        Objects.requireNonNull(entity, "Illegal parameter, entity must not be null.");
        repository.delete(entity);
    }

    @Override
    public void deleteById(ID id) throws EntityNotFoundException {
        shootIfDeletesDisabled();
        Objects.requireNonNull(id, "Illegal parameter, id must not be null.");
        try {
            this.repository.deleteById(id);
        } catch (EmptyResultDataAccessException var3) {
            throw new EntityNotFoundException(this.getType());
        }
    }

    @Override
    public boolean existsById(ID id) {
        shootIfQuerysDisabled();
        Objects.requireNonNull(id, "Illegal parameter, id must not be null.");
        return repository.existsById(id);
    }

    /*
     * Returns Entity
     */

    @Override
    public T _findById(ID id) throws EntityNotFoundException {
        shootIfQuerysDisabled();
        Objects.requireNonNull(id, "Illegal parameter, id must not be null.");
        return repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(getType().getSimpleName()));
    }

    @Override
    public List<T> _findAll() throws EmptyResourceException {
        shootIfQuerysDisabled();
        List<T> list = (List<T>) repository.findAll();
        if (list.isEmpty()) {
            throw new EmptyResourceException(getType().getSimpleName());
        }
        return list;
    }

    @Override
    public List<T> _findAll(Sort sort) throws EmptyResourceException {
        shootIfQuerysDisabled();
        List<T> list = (List<T>) repository.findAll(sort);
        if (list.isEmpty()) {
            throw new EmptyResourceException(getType().getSimpleName());
        }
        return list;
    }

    @Override
    public List<T> _findAllById(Iterable<ID> ids) throws EmptyResourceException {
        shootIfQuerysDisabled();
        List<T> list = (List<T>) repository.findAllById(ids);
        if (list.isEmpty()) {
            throw new EmptyResourceException(getType().getSimpleName());
        }
        return list;
    }

    @Override
    public T _save(T entity) throws StaleResourceException, InvalidEntityException {

        shootIfSavesDisabled();
        Object object = null;
        try {
            object = entityServiceTransactionWrapper.wrap(this.getClass(), "_doSave", ServiceReady.class, entity);

            // Here i need to check if there is a transaction before doing this!
            if (TransactionSynchronizationManager.isActualTransactionActive()) {
                // Returning from here i want to flush changes to the database and return up to date versions of them.
                if (entityManager.isJoinedToTransaction()) {
                    entityManager.flush();
                } else {
                    entityManager.joinTransaction();
                    entityManager.flush();
                }
            }

        } catch (Throwable e) {
            log.error("An error has happened: ",e);
            if (e instanceof StaleResourceException) throw (StaleResourceException) e;
            if (e instanceof InvalidEntityException) throw (InvalidEntityException) e;
            // todo If another exception ocurrs this will fail. Should log this.
        }
        return (T) object;
    }

    @Transactional(rollbackFor = { InvalidEntityException.class })
    public T _doSave(T entity) throws InvalidEntityException, StaleResourceException {

        Assert.notNull(entity, "Parameter cant be null.");

        if (!entity.autoValidate()) {
            entityServiceTransactionWrapper.setInvalidException(true);
            InvalidEntityException e = new InvalidEntityException(getType());
            entityServiceTransactionWrapper.putInExceptionsMap(EntityServiceTransactionWrapper.INVALID_EXCEPTION,e);
            throw e;
        }

        // Debug
        if (!TransactionSynchronizationManager.isActualTransactionActive()) {
            throw new IllegalStateException("Not in transaction.");
        }

        shootIfSavesDisabled();
        Assert.notNull(entity, "Illegal parameter, entity must not be null.");
        entityManager.setFlushMode(FlushModeType.COMMIT);
        /*
         * Flush mode type is set to COMMIT so it doesnt try to flush everything
         *  before queries made to the database.
         */
        if (!configuration.isMutable()) {
            try {
                return _findByT(entity);
                // todo Here i might get DisabledOperationException under wrong configuration.
            } catch (EntityNotFoundException e) {
                if (configuration.isComposite()) return compositeSave(entity);
                else return simpleSave(entity);
            }
        }
        if (configuration.isComposite()) return compositeSave(entity);
        else return simpleSave(entity);
    }

    @Override
    public List<T> _saveAll(Iterable<T> entities) throws StaleResourceException, InvalidEntityException {

        shootIfSavesDisabled();
        Object object = null;
        try {
            object = entityServiceTransactionWrapper.wrap(this.getClass(), "_doSaveAll", Iterable.class, entities);

            // Returning from here i want to flush changes to the database and return up to date versions of them.
            if (entityManager.isJoinedToTransaction()) {
                entityManager.flush();
            }

        } catch (Throwable e) {
            log.error("An error has happened: ",e);
            if (e instanceof StaleResourceException) throw (StaleResourceException) e;
            if (e instanceof InvalidEntityException) throw (InvalidEntityException) e;
            throw new RuntimeException(e);
        }
        return (List<T>) object;

    }

    @Transactional(rollbackFor = { InvalidEntityException.class })
    public List<T> _doSaveAll(Iterable<T> entities) throws StaleResourceException, InvalidEntityException {

        // Debug
        if (!TransactionSynchronizationManager.isActualTransactionActive()) {
            throw new IllegalStateException("Not in transaction.");
        };

        shootIfSavesDisabled();
        Assert.notNull(entities, "Entities must not be null!");
        List<T> result = new ArrayList<T>();
        for (T entity : entities) {
            result.add(this._doSave(entity));
        }
        return result;

    }


    @Override
    public Page<T> _findAll(Pageable pageable) throws EmptyResourceException {
        shootIfQuerysDisabled();
        Page<T> page = repository.findAll(pageable);
        if (page.isEmpty()) {
            throw new EmptyResourceException(getType().getSimpleName());
        }
        return page;
    }

    @Override
    public T _findOne(Specification<T> specification) throws EntityNotFoundException {
        shootIfQuerysDisabled();
        // todo This shit can throw detached entity passed to persist. Shit must be in context for this to search correctly.
        return repository.findOne(specification)
                .orElseThrow(() -> new EntityNotFoundException(getType().toString()));
    }

    @Override
    public List<T> _findAll(Specification<T> specification) {
        shootIfQuerysDisabled();
        return repository.findAll(specification);
    }

    @Override
    public List<T> _findAll(Specification<T> specification, Sort sort) {
        shootIfQuerysDisabled();
        return repository.findAll(specification, sort);
    }

    /*
     * This method should be implemented by those services that
     * have the mutable flag set to false.
     */
    public T _findByT(T entity) throws EntityNotFoundException {
        shootIfQuerysDisabled();
        Assert.notNull(entity, "Parameter should not be null!");
        throw new NotImplementedException();
    }

    ;

    /*
     * Returns Dto
     */

    @Override
    @Transactional
    public <S extends EntityVo<T, ID>> S findById(ID id) throws EntityNotFoundException {
        shootIfQuerysDisabled();
        return (S) getVoInstance().fromDomain(this._findById(id));
    }

    @Override
    @Transactional
    public <S extends EntityVo<T, ID>> List<S> findAll() throws EmptyResourceException {
        shootIfQuerysDisabled();
        return this._findAll().stream().map(entity -> (S) getVoInstance().fromDomain(entity)
        ).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public <S extends EntityVo<T, ID>> List<S> findAll(Sort sort) throws EmptyResourceException {
        shootIfQuerysDisabled();
        return this._findAll(sort).stream().map(entity -> (S) getVoInstance().fromDomain(entity)
        ).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public <S extends EntityVo<T, ID>> Page<S> findAll(Pageable pageable) throws EmptyResourceException {
        shootIfQuerysDisabled();
        return this._findAll(pageable).map(t -> (S) getVoInstance().fromDomain(t));
    }

    @Override
    @Transactional
    public <S extends EntityVo<T, ID>> List<S> findAllById(Iterable<ID> ids) throws EmptyResourceException {
        shootIfQuerysDisabled();
        return this._findAllById(ids).stream().map(entity -> (S) getVoInstance().fromDomain(entity)).collect(Collectors.toList());
    }

    @Override
    public <S extends EntityVo<T, ID>> S save(S entityDto) throws InvalidEntityException, StaleResourceException {

        shootIfSavesDisabled();
        Object object = null;
        try {
            object = entityServiceTransactionWrapper.wrap(this.getClass(), "doSave", EntityVo.class , entityDto);

        } catch (Throwable e) {
            log.error("An error has happened: ",e);
            if (e instanceof StaleResourceException) throw (StaleResourceException) e;
            if (e instanceof InvalidEntityException) throw (InvalidEntityException) e; // todo This needs to be tested. Need to save an invalid entity from this method. I think it's not posible.
            throw new IllegalStateException("Shouldnt have reached here.", e);
        }
        return (S) object;
    }

    @Transactional
    public <S extends EntityVo<T, ID>> S doSave(S entityDto) throws InvalidEntityException, StaleResourceException {

        // Debug
        //entityManager.isJoinedToTransaction();
        if (!TransactionSynchronizationManager.isActualTransactionActive()) {
            throw new IllegalStateException("Not in transaction.");
        };

        shootIfSavesDisabled();
        Objects.requireNonNull(entityDto, "Illegal parameter, entity must not be null.");
        S returnEntity = (S) getVoInstance().fromDomain(this._save(entityDto.toDomain()));
        return returnEntity;
    }

    @Override
    public <S extends EntityVo<T, ID>> List<S> saveAll(Iterable<S> entities) throws StaleResourceException, InvalidEntityException {

        shootIfSavesDisabled();
        Object object = null;
        try {
            object = entityServiceTransactionWrapper.wrap(this.getClass(), "doSaveAll", Iterable.class, entities);

        } catch (Throwable e) {
            log.error("An error has happened: ",e);
            if (e instanceof StaleResourceException) throw (StaleResourceException) e;
            if (e.getCause() instanceof InvalidEntityException) throw (InvalidEntityException) e.getCause(); // todo This needs to be tested. Need to save an invalid entity from this method. I think it's not posible.
            log.error("It was an unhandled exception: somethings broken! throwing runtimeException to wrap it. ", e);
            throw new RuntimeException(e);
        }
        return (List<S>) object;
    }

    @Transactional
    public <S extends EntityVo<T, ID>> List<S> doSaveAll(Iterable<S> entities) throws StaleResourceException, InvalidEntityException {

        // Debug
        if (!TransactionSynchronizationManager.isActualTransactionActive()) {
            throw new IllegalStateException("Not in transaction.");
        };

        shootIfSavesDisabled();
        Objects.requireNonNull(entities, "Illegal parameter, entities must not be null");
        entities.iterator()
                .forEachRemaining(
                        (entity) -> Objects.requireNonNull(entity, "Illegal parameter, entities iterable cant have a null entity")
                );
        List<T> domainEntities = new ArrayList<>();
        for (S s : entities) {
            T toDomain = s.toDomain();
            domainEntities.add(toDomain);
        }
        List<S> returnList = this._saveAll(domainEntities).stream().map(entity -> (S) getVoInstance().fromDomain(entity)).collect(Collectors.toList());
        return returnList;
    }

    @Override
    @Transactional
    public <S extends EntityVo<T, ID>> S findOne(Specification<T> specification) throws EntityNotFoundException {
        shootIfQuerysDisabled();
        return (S) getVoInstance().fromDomain(this._findOne(specification));
    }

    @Override
    @Transactional
    public <S extends EntityVo<T, ID>> List<S> findAll(Specification<T> specification) {
        shootIfQuerysDisabled();
        return this._findAll(specification).stream().map(entity -> (S) getVoInstance().fromDomain(entity)).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public <S extends EntityVo<T, ID>> List<S> findAll(Specification<T> specification, Sort sort) {
        shootIfQuerysDisabled();
        return this._findAll(specification, sort).stream().map(entity -> {
            S entityDTO = getVoInstance();
            return (S) entityDTO.fromDomain(entity);
        }).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public <S extends EntityVo<T, ID>> S findByT(S entityVo) throws EntityNotFoundException, InvalidEntityException {
        shootIfQuerysDisabled();
        Assert.notNull(entityVo, "Parameter should not be null!");
        return (S) getVoInstance().fromDomain(_findByT(entityVo.toDomain()));
    };

    /*
     * Abstract
     */

    private Field[] plusParentFieldsIfCorresponde(Class<?> clazz, Field[] previusFields) {
        if (implementsInterface(clazz.getSuperclass(),ServiceReady.class)) {
            Field[] fields = ArrayUtils.addAll(previusFields,clazz.getSuperclass().getDeclaredFields());
            return plusParentFieldsIfCorresponde(clazz.getSuperclass(), fields);
        }
        return previusFields;
    }

    protected abstract <S extends EntityVo<T, ID>> S getVoInstance();

    @SneakyThrows
    protected T attachObjects(T entity) throws InvalidEntityException {
        Assert.notNull(entity, "Illegal parameter, entity must not be null.");
        Class<?> clazz = entity.getClass();
        Field[] fieldList = clazz.getDeclaredFields();

        fieldList = plusParentFieldsIfCorresponde(clazz, fieldList);

        for (Field field : fieldList) {
            field.setAccessible(true);
            Class<?> fieldClass = field.getType();
            // Check if its a collection.
            if (implementsInterface(fieldClass, Collection.class)) {
                Collection<?> aCollection = (Collection<?>) field.get(entity);

                if (aCollection!=null) {
                    aCollection = (Collection<?>) aCollection.stream().map(o -> {
                        if (implementsInterface(o.getClass(), ServiceReady.class)) {
                            if (configuration.contains(o.getClass())) {
                                // Get the service and use it to fetch?
                                ServiceReady composite = (ServiceReady) o; // Cast the objet to serviceReady
                                try {

                                    if (configuration.alsoComposite(o.getClass()))
                                        // todo This should be configurable, what if i dont want to allow update in cascade?
                                        return configuration.getService(o.getClass())._doSave(composite); // Save the object with it's changes, returns attached to context.
                                        // todo Here i might get DisabledOperationException under wrong configuration.
                                    else
                                        return configuration.getService(o.getClass())._findById(composite.getId()); // Go find it on the service by it's id, returns attached to context.
                                        // todo Here i might get DisabledOperationException under wrong configuration.

                                } catch (StaleResourceException | InvalidEntityException | EntityNotFoundException e) {
                                    // Here im translating this to a runtime excepcion, since checked excepcion cant scape this block
                                    throw new RuntimeException(e);
                                    // Todo log this errors.
                                }
                            }
                        }
                        return o;
                    }).collect(Collectors.toCollection(new OwlCollectionsFactory(field)));
                }

                field.set(entity,aCollection);

            } else {
                // Check if it extends ServiceReady.
                if (implementsInterface(fieldClass, ServiceReady.class)) {
                    // Check if it's in the configuration.
                    if (configuration.contains(fieldClass)) {
                        // Get the service and use it to fetch?
                        ServiceReady composite = (ServiceReady) field.get(entity); // Get the composite object instance.
                        if (composite!=null) {
                            try {
                                if (configuration.alsoComposite(fieldClass))
                                    // todo This should be configurable, what if i dont want to allow update in cascade?
                                    field.set(entity, configuration.getService(fieldClass)._doSave(composite)); // Save the object with it's changes, returns attached to context.
                                    // todo Here i might get DisabledOperationException under wrong configuration.
                                else
                                    field.set(entity, configuration.getService(fieldClass)._findById(composite.getId())); // Go find it on the service by it's id, returns attached to context.
                                // todo Here i might get DisabledOperationException under wrong configuration.
                            } catch (EntityNotFoundException e) {
                                throw new InvalidEntityException(entity.getClass(), e.getMessage());
                            }
                        }
                    }
                }
            }
        }
        return entity;
    }

    /*
     * Methods
     */

    public Class<T> getType() {
        ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
        return (Class<T>) type.getActualTypeArguments()[0];
    }

    @Override
    public String getServiceName() {
        return this.getClass().getInterfaces()[0].getSimpleName();
    }

    /*
     * Private Methods
     */

    private T simpleSave(T entity) throws InvalidEntityException, StaleResourceException {
        Assert.notNull(entity, "Illegal parameter, entity must not be null.");
        if (configuration.shouldFindBeforeSave()) {
            try {
                T fetch = _findByT(entity); // todo Here i might get DisabledOperationException under wrong configuration.
                fetch = MergingUtils.mergeEntity(fetch, entity);
                entity = attachObjects(fetch);
            } catch (EntityNotFoundException e) {
                attachObjects(entity);
            }
        } else {
            attachObjects(entity);
        }
        return persistItAlready(entity);
    }

    private T compositeSave(T entity) throws InvalidEntityException, StaleResourceException {
        Assert.notNull(entity, "Illegal parameter, entity must not be null.");
        if (entity.getId() != null) { // This is an update.
            T attached = repository.findById(entity.getId()).orElseThrow(() -> new InvalidEntityException(getType(), "Entity had an invalid Id. (Not found on the server)")); // Now the object is in context.
            attached = MergingUtils.mergeEntity(attached, entity); // Still in context, but some of the composite objects may be dettached.
            attachObjects(attached);
            return persistItAlready(attached);
        } else { // This has to be persisted.
            if (configuration.shouldFindBeforeSave()) {
                try {
                    T fetch = _findByT(entity); // todo Here i might get DisabledOperationException under wrong configuration.
                    fetch = MergingUtils.mergeEntity(fetch, entity); // todo Change this, this can edit an inmutable entity cause it skips the check
                    entity = attachObjects(fetch);
                } catch (EntityNotFoundException e) {
                    attachObjects(entity);
                }
            } else {
                attachObjects(entity);
            }
            return persistItAlready(entity);
        }
    }

    private T persistItAlready(T entity) throws StaleResourceException, InvalidEntityException {
        if (!entity.autoValidate()) {
            throw new InvalidEntityException(getType());
        }
        try {
            /*
             * If entity ID type is UUID and id is null, assign it a value.
             */
            generateUUIDifNeeded(entity);
            return repository.save(entity);
        } catch (OptimisticLockException | ConcurrencyFailureException e) { // todo Here OptimisticLockException is thrown by something?
            entityServiceTransactionWrapper.setStaleException(true);
            StaleResourceException staleEx = new StaleResourceException(e.getMessage());
            entityServiceTransactionWrapper.putInExceptionsMap(EntityServiceTransactionWrapper.STALE_EXCEPTION, staleEx);
            throw staleEx; // todo Here i can throw more information about the error.
        }
    }


    /*
     * Protected Methods
     */

    protected void shootIfQuerysDisabled() throws DisabledOperationException {
        if (!configuration.areQuerysAllowed()) throw new DisabledOperationException("Querys are disabled in this service.");
    }

    protected void shootIfSavesDisabled() throws DisabledOperationException {
        if (!configuration.areSavesAllowed()) throw new DisabledOperationException("Saves are disabled in this service.");
    }

    protected void shootIfDeletesDisabled() throws DisabledOperationException {
        if (!configuration.areDeletesAllowed()) throw new DisabledOperationException("Deletes are disabled in this service.");
    }

    /*
     * Private methods
     */

    private void generateUUIDifNeeded(T entity) {
        Class<?> entityClass = entity.getClass();
        if (implementsInterface(entityClass, ServiceReady.class)) {
            Class<?> implementingClass = getServiceReadyImplementingClass(entityClass);
            Type[] types = implementingClass.getGenericInterfaces();
            for (Type type : types) {
                if (((ParameterizedType)type).getRawType().equals(ServiceReady.class)) {
                    Type[] actualTypeArguments = ((ParameterizedType)type).getActualTypeArguments();
                    if (actualTypeArguments[0].equals(UUID.class)) {
                        try {
                            Method getIdMethod = ServiceReady.class.getMethod("getId");
                            getIdMethod.setAccessible(true);
                            Object methodReturn = getIdMethod.invoke(entity);
                            if (methodReturn==null) {
                                Method setIdMethod = ServiceReady.class.getMethod("setId", Serializable.class);
                                setIdMethod.setAccessible(true);
                                setIdMethod.invoke(entity,UUID.randomUUID());
                            }
                        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                            e.printStackTrace();
                            log.error("Something went wrong while setting UUID", e);
                        }
                    }
                }
            }
        }
    }

    private boolean implementsServiceReadyDirectly(Class<?> clazz) {
        Class<?>[] implementedInterfaces = clazz.getInterfaces();
        for (Class<?> implementedInterface : implementedInterfaces) {
            if (implementedInterface.equals(ServiceReady.class)) return true;
        }
        return false;
    }

    private Class<?> getServiceReadyImplementingClass(Class<?> clazz) {
        Class<?> implementingClazz = null;
        if (implementsServiceReadyDirectly(clazz)) {
            implementingClazz = clazz;
        } else {
            implementingClazz = getServiceReadyImplementingClass(clazz.getSuperclass());
        }
        return implementingClazz;
    }

}

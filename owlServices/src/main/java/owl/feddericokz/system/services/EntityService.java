package owl.feddericokz.system.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.prepost.PreAuthorize;
import owl.feddericokz.system.domains.EntityVo;
import owl.feddericokz.system.domains.ServiceReady;
import owl.feddericokz.system.exceptions.*;

import java.io.Serializable;
import java.util.List;

/**
 * Contract of the GenericService, it has a lot of CRUD operations with return
 * Entities and Dtos.
 * @param <T>
 * @param <ID>
 */
public interface EntityService<T extends ServiceReady<ID>, ID extends Serializable> {

    /*
     * Methods CrudRepository
     */

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'countAll')")
    Long countAll();

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'delete')")
    void delete(T entity) throws DisabledOperationException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'deleteById')")
    void deleteById(ID id) throws DisabledOperationException, EntityNotFoundException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'existsById')")
    boolean existsById(ID id);

    /*
     * Returns Entity
     */

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'findById')")
    T _findById(ID id) throws EntityNotFoundException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'findAll')")
    List<T> _findAll() throws EmptyResourceException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'findAll')")
    List<T> _findAll(Sort sort) throws EmptyResourceException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'findAllById')")
    List<T> _findAllById(Iterable<ID> ids) throws EmptyResourceException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'save')")
    T _save(T entity) throws StaleResourceException, InvalidEntityException;

    /**
     * todo I hope i remember the difference between this and _save by the time i write this.
     *  hint: it has to do with the flush.
     * @param entity
     * @return
     * @throws InvalidEntityException
     * @throws StaleResourceException
     */
    T _doSave(T entity) throws InvalidEntityException, StaleResourceException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'saveAll')")
    List<T> _saveAll(Iterable<T> entities) throws StaleResourceException, InvalidEntityException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'findAll')")
    Page<T> _findAll(Pageable pageable) throws EmptyResourceException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'findOne')")
    T _findOne(Specification<T> specification) throws EntityNotFoundException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'findAll')")
    List<T> _findAll(Specification<T> specification);

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'findAll')")
    List<T> _findAll(Specification<T> specification, Sort sort);

    T _findByT(T entity) throws EntityNotFoundException;;

    /*
     * Returns Dto
     */

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'findById')")
    <S extends EntityVo<T,ID>> S findById(ID id) throws EntityNotFoundException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'findAll')")
    <S extends EntityVo<T,ID>> List<S> findAll() throws EmptyResourceException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'findAll')")
    <S extends EntityVo<T,ID>> List<S> findAll(Sort sort) throws EmptyResourceException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'findAllById')")
    <S extends EntityVo<T,ID>> List<S> findAllById(Iterable<ID> ids) throws EmptyResourceException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'save')")
    <S extends EntityVo<T,ID>> S save(S entityDTO) throws InvalidEntityException, StaleResourceException;

    /**
     * todo I hope i remember the difference between this and _save by the time i write this.
     *  hint: it has to do with the flush.
     * @param entity
     * @return
     * @throws InvalidEntityException
     * @throws StaleResourceException
     */
    <S extends EntityVo<T,ID>> S doSave(S entity) throws InvalidEntityException, StaleResourceException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'saveAll')")
    <S extends EntityVo<T,ID>> List<S> saveAll(Iterable<S> entities) throws StaleResourceException, InvalidEntityException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'findAll')")
    <S extends EntityVo<T,ID>> Page<S> findAll(Pageable pageable) throws EmptyResourceException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'findOne')")
    <S extends EntityVo<T,ID>> S findOne(Specification<T> specification) throws EntityNotFoundException;

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'findAll')")
    <S extends EntityVo<T,ID>> List<S> findAll(Specification<T> specification);

    @PreAuthorize("@permissionsService.hasPermission(authentication,this.getServiceName(),'findAll')")
    <S extends EntityVo<T,ID>> List<S> findAll(Specification<T> specification, Sort sort);

    // todo Spring Security here?
    <S extends EntityVo<T,ID>> S findByT(S entityVo) throws EntityNotFoundException, InvalidEntityException;

    /*
     * Utils
     */

    String getServiceName();

}

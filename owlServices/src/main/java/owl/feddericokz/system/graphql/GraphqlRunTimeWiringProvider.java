package owl.feddericokz.system.graphql;

import graphql.schema.idl.TypeRuntimeWiring;

import java.util.List;

/**
 * This interface has the same purpose as the @GraphqlDefinitionsProvider
 * but it stitches RunTimeWirings together.
 */
public interface GraphqlRunTimeWiringProvider {

    /**
     * @return Returns a List of pre-built TypeRuntimeWirings,
     * however they may be built.
     */
    List<TypeRuntimeWiring> getTypeRuntimeWirings();

}

package owl.feddericokz.system.graphql;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.util.Assert;
import owl.feddericokz.system.domains.EntityVo;
import owl.feddericokz.system.exceptions.EntityNotFoundException;
import owl.feddericokz.system.services.EntityService;

public class EntityByIdDataFetcher<S extends EntityVo> implements DataFetcher<S> {

    /*
     * Fields
     */

    private EntityService entityService;

    /*
     * Constructors
     */

    public EntityByIdDataFetcher(EntityService entityService) {
        this.entityService = entityService;
    }

    /*
     * Methods
     */

    @Override
    public S get(DataFetchingEnvironment environment) {
        String id = environment.getArgument("id");
        Assert.notNull(id,"Id cant be null!");
        try {
            return (S) entityService.findById(Long.valueOf(id));
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

}

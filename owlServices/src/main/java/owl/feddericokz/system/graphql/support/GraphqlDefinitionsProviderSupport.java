package owl.feddericokz.system.graphql.support;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import graphql.language.Definition;
import graphql.language.Document;
import graphql.parser.Parser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.Assert;
import owl.feddericokz.system.graphql.GraphqlDefinitionsProvider;

import java.io.IOException;
import java.net.URL;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GraphqlDefinitionsProviderSupport implements GraphqlDefinitionsProvider {

    /*
     * Fields
     */

    private String schemaPath = "graphql/schema.graphqls";
    private ClassLoader classLoader;

    /*
     * Constructors
     */

    public GraphqlDefinitionsProviderSupport(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    public GraphqlDefinitionsProviderSupport(String schemaPath) {
        this.schemaPath = schemaPath;
    }

    /*
     * Methods
     */

    @Override
    public List<Definition> getDefinitions() throws IOException {
        Assert.notNull(schemaPath,"Path cant be null!");
        Assert.notNull(classLoader,"Classloader must be set!");
        URL schemaUrl = classLoader.getResource(schemaPath);
        assert schemaUrl != null;
        String schemaInput = Resources.toString(schemaUrl, Charsets.UTF_8);
        Parser graphQlParser = new Parser();
        Document document = graphQlParser.parseDocument(schemaInput);
        return document.getDefinitions();
    }

}

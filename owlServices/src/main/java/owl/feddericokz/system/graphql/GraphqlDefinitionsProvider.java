package owl.feddericokz.system.graphql;

import graphql.language.Definition;

import java.util.List;

/**
 * This interface defines the contract for a provider that allows
 * GraphQL definitions to be declared in multiple files and have them
 * automatically stitched together.
 */
public interface GraphqlDefinitionsProvider {

    /**
     * Gets GraphQL Definitions wherever they may be.
     * @return Returns a List of some implementation of @Definition
     * @throws Exception, Here i expect some Exception to be thrown by the implementation class,
     * IO Maybe?
     */
    public List<Definition> getDefinitions() throws Exception;

}

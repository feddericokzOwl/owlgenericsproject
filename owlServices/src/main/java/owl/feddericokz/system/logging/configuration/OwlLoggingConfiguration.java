package owl.feddericokz.system.logging.configuration;

import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import owl.feddericokz.system.logging.OwlLoggingAspect;

@Configuration
@ComponentScan(basePackageClasses = OwlLoggingAspect.class)
public class OwlLoggingConfiguration {

    @Pointcut("execution(* owl.feddericokz..*(..)))")
    public void owlServiceMethods() {}

}

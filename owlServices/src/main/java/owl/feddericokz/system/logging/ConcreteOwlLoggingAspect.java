package owl.feddericokz.system.logging;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ConcreteOwlLoggingAspect extends OwlLoggingAspect {

    private static final Logger log = LoggerFactory.getLogger(ConcreteOwlLoggingAspect.class);

    public ConcreteOwlLoggingAspect() {
    }

    @Around("owl.feddericokz.system.logging.configuration.OwlLoggingConfiguration.owlServiceMethods()")
    public Object logInfoOfMethodExcecution(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        return super.doLogInfoOfMethodExcecution(proceedingJoinPoint, log);
    }

}

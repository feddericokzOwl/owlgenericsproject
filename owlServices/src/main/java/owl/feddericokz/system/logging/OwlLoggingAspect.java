package owl.feddericokz.system.logging;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.Data;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StopWatch;

import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static owl.feddericokz.system.utils.OwlReflectionUtils.implementsInterface;

@Data
@Aspect
public abstract class OwlLoggingAspect {

    /*
     * Fields
     */

    @Value("${owl.logging.enable-owl-logging-aspect:true}")
    private boolean turnedOn;

    /*
     * Methods
     */

    public Object logInfoOfMethodExcecution(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

        return doLogInfoOfMethodExcecution(proceedingJoinPoint, LoggerFactory.getLogger(OwlLoggingAspect.class));

    }

    private static void registerCustomSerializarAndDeserializerOnMapper(ObjectMapper mapper) {
        SimpleModule module = new SimpleModule();
        module.addSerializer(new InputStreamResourceSerializer(InputStreamResource.class));
        mapper.registerModule(module);
    }

    protected Object doLogInfoOfMethodExcecution(ProceedingJoinPoint proceedingJoinPoint, Logger log) throws Throwable {
        Assert.notNull(log,"Logger cant be null, dude!");

        // What is a proceedingJointPoint? Looks like i can get the calling method signature out of it.

        MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();

        // Get intercepted method details.

        String className = methodSignature.getDeclaringType().getSimpleName();
        String methodName = methodSignature.getName();

        // Get the args.

        Object[] args = proceedingJoinPoint.getArgs();

        // I use this to print the objets to log.

        if (turnedOn) log.info("About to execute method >> "+ className + "." + methodName);

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        if (args.length > 0) {
            for (Object arg : args) {
                if (arg!=null) {
                    if (!(implementsInterface(arg.getClass(), HttpServletResponse.class))) // No need to serialize this parameter, since its like a bean.
                        if (turnedOn) log.info("Execution parameter of " + className + "." + methodName + " >> " + " Class: " + arg.getClass().getSimpleName() + " > " + mapper.writeValueAsString(arg));
                }
            }
        }

        // Measure method execution time.

        final StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Object result;
        try {
            result = proceedingJoinPoint.proceed();
        } catch (Throwable e) {
            if (turnedOn) log.info("Exited method with error >> "+ className + "." + methodName);
            if (turnedOn) log.error("Exception message >> " + e.getMessage());
            if (turnedOn) log.error("Exception stacktrace >> ", e);
            throw e;
        }
        stopWatch.stop();
        if (turnedOn) log.info("Exited method succesfully >> "+ className + "." + methodName);

        // Log output.

        if (turnedOn) log.info("Execution time of " + className + "." + methodName + " >> " + stopWatch.getTotalTimeMillis() + " ms");

        registerCustomSerializarAndDeserializerOnMapper(mapper);

        if (turnedOn) log.info("Execution result of " + className + "." + methodName + " >> " + mapper.writeValueAsString(result));

        // Then return execution result.

        return result;
    }

    /*
     * A class!
     */

    public static class InputStreamResourceSerializer extends StdSerializer<InputStreamResource> {

        public InputStreamResourceSerializer() {
            this(null);
        }

        public InputStreamResourceSerializer(Class<InputStreamResource> t) {
            super(t);
        }

        @Override
        public void serialize(
                InputStreamResource value, JsonGenerator jgen, SerializerProvider provider)
                throws IOException, JsonProcessingException {

            jgen.writeStartObject();
            jgen.writeStringField("description", value.getDescription());
            jgen.writeEndObject();
        }

    }

}

package owl.feddericokz.system.exceptions;

public class BadQueryException extends Exception {

    public BadQueryException() {
    }

    public BadQueryException(String message) {
        super(message);
    }

}

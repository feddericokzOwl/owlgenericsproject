package owl.feddericokz.system.exceptions;


public class EntityNotFoundException extends Exception {

    public EntityNotFoundException(String type) {
        super("Entity of type: ["+type+"] wasnt found on the server.");
    }

    public EntityNotFoundException(Class<?> clazz) {
        this(clazz.getSimpleName());
    }

}

package owl.feddericokz.system.exceptions;

public class EmptyResourceException extends Exception {

    public EmptyResourceException(String type) {
        super("Resource of type: ["+type+"] is empty or you are out of paging bounds.");
    }

    public EmptyResourceException(Class<?> clazz) {
        this(clazz.getSimpleName());
    }

}

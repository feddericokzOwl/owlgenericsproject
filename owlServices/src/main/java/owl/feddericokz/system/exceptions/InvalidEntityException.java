package owl.feddericokz.system.exceptions;

public class InvalidEntityException extends Exception {

    public InvalidEntityException(String type, String message) {
        super("Entity of type: ["+type+"] failed validation." + (message!=null ? "Message: "+message : "") );
    }

    public InvalidEntityException(Class<?> type) {
        this(type.getSimpleName(),null);
    }

    public InvalidEntityException(Class<?> type, String message) {
        this(type.getSimpleName(), message);
    }

}

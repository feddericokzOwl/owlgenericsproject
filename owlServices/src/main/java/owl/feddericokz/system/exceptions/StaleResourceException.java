package owl.feddericokz.system.exceptions;

public class StaleResourceException extends Exception {

    public StaleResourceException() {
    }

    public StaleResourceException(String message) {
        super(message);
    }

}

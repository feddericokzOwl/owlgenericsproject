package owl.feddericokz.system.exceptions;

public class DisabledOperationException extends RuntimeException {

    public DisabledOperationException(String message) {
        super(message);
    }

}

package owl.feddericokz.system.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;

/**
 * Its nice to have the JpaSpecificationExecutor along with The PagingAndSortingRepository
 * So here it is.
 * @param <T>
 * @param <ID>
 */
@NoRepositoryBean
public interface EntityRepository<T, ID extends Serializable>
        extends PagingAndSortingRepository<T,ID>, JpaSpecificationExecutor<T> {

    /*
     * Methods
     */

    /**
     * You know, in case you wanna flush.
     */
    void flush();

}

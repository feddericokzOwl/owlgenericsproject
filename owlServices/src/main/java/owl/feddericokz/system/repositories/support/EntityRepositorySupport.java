package owl.feddericokz.system.repositories.support;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import owl.feddericokz.system.repositories.EntityRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

/**
 * Look at all this room for customizing shit right?
 * @param <T>
 * @param <ID>
 */
public abstract class EntityRepositorySupport<T,ID extends Serializable>
        extends SimpleJpaRepository<T,ID>
        implements EntityRepository<T,ID> {

    /*
     * Context
     */

    @PersistenceContext
    private EntityManager entityManager;

    /*
     * Constructor
     */

    public EntityRepositorySupport(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
    }

    /*
     * Methods
     */

    @Override
    public void flush() {
        entityManager.flush();
    }

}

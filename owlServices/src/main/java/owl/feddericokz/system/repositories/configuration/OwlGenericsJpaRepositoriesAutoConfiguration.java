package owl.feddericokz.system.repositories.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import owl.feddericokz.system.repositories.support.EntityRepositorySupport;

@Configuration
@EnableJpaRepositories(repositoryBaseClass = EntityRepositorySupport.class)
public class OwlGenericsJpaRepositoriesAutoConfiguration {
}

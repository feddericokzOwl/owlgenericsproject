package owl.feddericokz.system.utils;

import lombok.SneakyThrows;
import org.springframework.util.Assert;
import owl.feddericokz.system.domains.EntityVo;
import owl.feddericokz.system.domains.EnumVo;
import owl.feddericokz.system.domains.ServiceReady;
import owl.feddericokz.system.domains.ServiceVo;
import owl.feddericokz.system.domains.support.OwlCollectionsFactory;
import owl.feddericokz.system.exceptions.InvalidEntityException;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.stream.Collectors;

import static owl.feddericokz.system.utils.OwlReflectionUtils.implementsInterface;


/**
 * Utility class used to merge objects without knowledge of it fields.
 */
public class MergingUtils {

    /*
     * Constants
     */

    /**
     * "FULL" Merge takes null values into account
     * and merges them as well.
     */
    public final static String FULL = "FULL";

    /**
     * "PARTIAL" Merge ignores null values.
     */
    public final static String PARTIAL = "PARTIAL";

    /*
     * Methods
     */

    /**
     *  Merges 2 Entities.
     * @param entity Entity that will receive the update.
     * @param update Object whose values will be put in the entity.
     * @param typeOfMerge "FULL" or "PARTIAL", declared constants can/should be used here.
     * @return Returns the merged entity.
     */
    @SneakyThrows
    public static <T extends ServiceReady> T mergeEntity(T entity, T update, String typeOfMerge) {
        Assert.notNull(entity, "Parameter cant be null!");
        Assert.notNull(update, "Parameter cant be null!");
        Assert.notNull(typeOfMerge, "Parameter cant be null!");
        Class<T> typeClass = (Class<T>) entity.getClass();
        Field[] typeFields = typeClass.getDeclaredFields();
        for (Field field : typeFields) {
            if (!field.getName().equals("id") && !field.getName().equals("versionTag")) {
                field.setAccessible(true);
                Object updateValue = field.get(update); // SneakyThrows
                Object entityValue = field.get(entity); // SneakyThrows
                if (typeOfMerge.equals(MergingUtils.PARTIAL)) {
                    if (updateValue != null && entityValue == null) {
                        field.set(entity, updateValue); // SneakyThrows
                    } else if (updateValue != null && !entityValue.equals(updateValue)) {
                        field.set(entity, updateValue); // SneakyThrows
                    }
                } else field.set(entity, updateValue); // SneakyThrows
            }
        }
        return entity;
    }

    /**
     * Wrapper to avoid writing "FULL".
     * @return Returns the merged entity.
     */
    public static <T extends ServiceReady> T mergeEntity(T entity, T update) {
        return mergeEntity(entity, update, FULL);
    }

    /**
     *  Merges a Vo into an entity.
     * @param entity Entity that will receive the update.
     * @param update Dto containing the values to update.
     * @param typeOfMerge "FULL" or "PARTIAL", declared constants can/should be used here.
     * @return Returns the merged entity.
     */
    @SneakyThrows
    public static <T extends ServiceReady, S extends EntityVo> T mergeVoIntoEntity(T entity, S update, String typeOfMerge) {
        Assert.notNull(entity, "Parameter cant be null!");
        Assert.notNull(update, "Parameter cant be null!");
        Assert.notNull(typeOfMerge, "Parameter cant be null!");
        Class<T> typeClass = (Class<T>) entity.getClass();
        Field[] typeFields = typeClass.getDeclaredFields();
        Class<S> voClass = (Class<S>) update.getClass();
        for (Field field : typeFields) {
            if (!field.getName().equals("id") && !field.getName().equals("versionTag")) {
                field.setAccessible(true);
                Field updateField = voClass.getDeclaredField(field.getName());
                updateField.setAccessible(true);
                Class<?> clazz = updateField.get(update) != null ? updateField.get(update).getClass() : null;
                Object updateValue = updateField.get(update);
                ServiceVo voUpdateValue = null;
                if (implementsInterface(clazz, EntityVo.class)) voUpdateValue = (EntityVo) updateValue;
                if (implementsInterface(clazz, EnumVo.class)) voUpdateValue = (EnumVo) updateValue;
                Object entityValue = field.get(entity);
                if (typeOfMerge.equals(MergingUtils.PARTIAL)) {
                    if (updateValue != null) {
                        if (!implementsInterface(clazz,Collection.class)) {
                            if (entityValue == null) {
                                if (voUpdateValue != null)
                                    field.set(entity, voUpdateValue.toDomain());
                                else
                                    field.set(entity, updateValue);
                            } else if (!entityValue.equals(updateValue)) {
                                if (voUpdateValue != null)
                                    field.set(entity, voUpdateValue.toDomain());
                                else
                                    field.set(entity, updateValue);
                            }
                        } else {
                            mergeCollection(entity, field, (Collection<?>) updateValue);
                        }
                    }
                } else {
                    if (!implementsInterface(clazz,Collection.class)) {
                        if (voUpdateValue != null)
                            field.set(entity, voUpdateValue.toDomain());
                        else
                            field.set(entity, updateValue);
                    } else {
                        mergeCollection(entity, field, (Collection<?>) updateValue);
                    }
                }
            }
        }
        return entity;
    }

    /**
     * Wrapper to avoid writing "FULL".
     * @return Returns the merged entity.
     */
    public static <T extends ServiceReady, S extends EntityVo> T mergeVoIntoEntity(T entity, S update){
        return mergeVoIntoEntity(entity, update, FULL);
    }

    /**
     *  Merges 2 Vo's together.
     * @param entity Vo that will receive the update.
     * @param update Vo containing the values to update.
     * @param typeOfMerge "FULL" or "PARTIAL", declared constants can/should be used here.
     * @return Returns the merged entity.
     */
    @SneakyThrows
    public static <S extends EntityVo> S mergeVo(S entity, S update, String typeOfMerge) {
        Class<S> dtoClass = (Class<S>) update.getClass();
        Field[] typeFields = dtoClass.getDeclaredFields();
        for (Field field : typeFields) {
            if (!field.getName().equals("id") && !field.getName().equals("versionTag")) {
                field.setAccessible(true);
                Field updateField = dtoClass.getDeclaredField(field.getName());
                updateField.setAccessible(true);
                //Class<?> clazz = updateField.get(update) != null ? updateField.get(update).getClass() : null;
                Object updateValue = updateField.get(update);
                Object entityValue = field.get(entity);
                if (typeOfMerge.equals(MergingUtils.PARTIAL)) {
                    if (entityValue == null) {
                        if (updateValue != null)
                            field.set(entity, updateValue);
                    } else if (!entityValue.equals(updateValue)) {
                        if (updateValue != null)
                            field.set(entity, updateValue);
                    }
                } else {
                    field.set(entity, updateValue);
                }
            }
        }
        return entity;
    }

    /**
     * Wrapper to avoid writing "FULL".
     * @return Returns the merged entity.
     */
    public static <S extends EntityVo> S mergeVo(S entity, S update) {
        return mergeVo(entity,update,FULL);
    }

    /*
     * Private Methods
     */

    private static <T extends ServiceReady> void mergeCollection(T entity, Field field, Collection<?> updateValue) throws IllegalAccessException, InvalidEntityException {
        Collection<?> voCollection = updateValue;
        // I have to stream this collection, check if it's a EntityVo or a EnumVo, and call to domain.
        try {
            if (voCollection != null && !voCollection.isEmpty())
                field.set(entity,
                        voCollection.stream().map(o -> {
                            /*try {*/
                                if (implementsInterface(o.getClass(), EntityVo.class)) { // Check if it implements EntityVo
                                    return ((EntityVo) o).toDomain();
                                } else if (implementsInterface(o.getClass(), EnumVo.class)) { // Check if it implements EnumVo
                                    return ((EnumVo) o).toDomain();
                                } else {
                                    return o;
                                }
                            /*} catch (InvalidEntityException e) {
                                throw new RuntimeException(e);
                            }*/
                        }).collect(Collectors.toCollection(new OwlCollectionsFactory(field)))
                );
        } catch (RuntimeException e) {
            if (e.getCause() instanceof InvalidEntityException)
                throw (InvalidEntityException) e.getCause();
        }
    }

}

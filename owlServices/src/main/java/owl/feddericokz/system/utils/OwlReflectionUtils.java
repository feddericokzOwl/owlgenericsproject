package owl.feddericokz.system.utils;

import org.springframework.util.Assert;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class OwlReflectionUtils {

    /*
     *
     */
    public static List<Class<?>> extractImplementedInterfaces(Class<?> clazz) {
        Assert.notNull(clazz, "Cant extract interface of null.");
        Type[] types = clazz.getGenericInterfaces();
        if (types.length==0) {
            Class<?> superClazz = clazz.getSuperclass();
            if (superClazz==null) return null;
            types = superClazz.getGenericInterfaces();
        }
        List<ParameterizedType> parameterizedTypes = new ArrayList<>();
        if (types.length > 0) {
            for (Type type : types) {
                if (type instanceof ParameterizedType) {
                    parameterizedTypes.add((ParameterizedType) type);
                }
            }
        }
        List<Class<?>> returnList = new ArrayList<>();
        if (!parameterizedTypes.isEmpty()) {
            for (ParameterizedType parameterizedType : parameterizedTypes) {
                Type type = parameterizedType.getRawType();
                returnList.add((Class) type);
            }
        }

        if (clazz.getSuperclass()!=null) {
            List<Class<?>> resultList = extractImplementedInterfaces(clazz.getSuperclass());
            if (resultList!=null && !resultList.isEmpty()) returnList.addAll(resultList);
        }
        if (returnList.isEmpty()) return null;
        return returnList;
    }

    /*
     *
     */
    public static boolean implementsInterface(Class<?> sourceClazz, Class<?> targetClazz) {
        if (sourceClazz==null || targetClazz == null) return false;
        List<Class<?>> interfacesList = extractImplementedInterfaces(sourceClazz);
        if (interfacesList!=null && !interfacesList.isEmpty()) {
            for (Class<?> aClass : interfacesList) {
                if (aClass.equals(targetClazz)) return true;
            }
        }
        return false;
    }

    public static boolean subclassOf(Class<?> sourceClazz, Class<?> targetClazz) {
        return sourceClazz.getSuperclass() != null && sourceClazz.getSuperclass().equals(targetClazz);
    }

}

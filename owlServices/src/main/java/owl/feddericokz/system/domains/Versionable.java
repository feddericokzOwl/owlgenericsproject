package owl.feddericokz.system.domains;

import java.time.LocalDateTime;

/**
 * This interface forces classes to have a version field
 * which intended purpose is to be used as an optimistic lock.
 */
public interface Versionable {

    /**
     * @return Returns the last time the object was updated.
     */
    LocalDateTime getVersionTag();

    void setVersionTag(LocalDateTime versionTag);

}

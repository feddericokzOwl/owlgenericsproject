package owl.feddericokz.system.domains.support;

import lombok.EqualsAndHashCode;
import lombok.Setter;
import owl.feddericokz.system.domains.ServiceEnum;

import javax.persistence.*;
import java.io.Serializable;

@MappedSuperclass
@EqualsAndHashCode(callSuper = false)
@Setter
public abstract class ServiceEnumSupport<ID extends Serializable> implements ServiceEnum<ID> {

    /*
     * Fields
     */

    private ID id;

    /*
     * Getters
     */

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id") // todo Can this be taken from a properties file?
    public ID getId() {
        return this.id;
    }


}

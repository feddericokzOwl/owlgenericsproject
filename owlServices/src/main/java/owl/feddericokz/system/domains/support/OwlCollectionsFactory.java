package owl.feddericokz.system.domains.support;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Supplier;

public class OwlCollectionsFactory implements Supplier {

    /*
     * Fields
     */

    private Field field;

    /*
     * Constructors
     */

    public OwlCollectionsFactory(Field field) {
        this.field = field;
    }

    @Override
    public Object get() {
        if (field.getType().equals(List.class)) {
            return new ArrayList<>();
        }
        if (field.getType().equals(ArrayList.class)) {
            return new ArrayList<>();
        }
        if (field.getType().equals(Map.class)) {
            return new HashMap<>();
        }
        if (field.getType().equals(HashMap.class)) {
            return new HashMap<>();
        }
        if (field.getType().equals(Set.class)) {
            return new HashSet<>();
        }
        if (field.getType().equals(HashSet.class)) {
            return new HashSet<>();
        }
        throw new IllegalArgumentException();
    }

}

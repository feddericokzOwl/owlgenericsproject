package owl.feddericokz.system.domains;

import java.io.Serializable;

/**
 * Interface that must be implemented by DataTransferObjects
 * that intend to work with the System Generics.
 * @param <T>
 * @param <ID>
 */
public interface EntityVo<T extends ServiceReady<ID>,ID extends Serializable> extends Identifiable<ID>, ServiceVo<T>, Versionable {
}

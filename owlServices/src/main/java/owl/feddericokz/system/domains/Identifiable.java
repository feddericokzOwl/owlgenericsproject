package owl.feddericokz.system.domains;

import java.io.Serializable;

/**
 * This interface forces classes to have an unique identifier.
 * @param <ID>
 */
public interface Identifiable<ID extends Serializable> {

    /**
     * @return Returns the unique identifier of the object.
     */
    ID getId();

    void setId(ID id);

}

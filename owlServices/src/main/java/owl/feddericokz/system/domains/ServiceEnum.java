package owl.feddericokz.system.domains;

import java.io.Serializable;

/**
 * The only purpose of this interface is to identify classes
 * that are database enumerates and enforce required fields.
 * @param <ID>
 */
public interface ServiceEnum<ID extends Serializable> extends Identifiable<ID> {
}

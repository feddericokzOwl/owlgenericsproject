package owl.feddericokz.system.domains;

import java.io.Serializable;

/**
 * This interface is a wrapper to implement all needed interfaces the system needs to work, all at once.
 * @param <ID>
 */
public interface ServiceReady<ID extends Serializable> extends Validable, Versionable, Identifiable<ID> {
}

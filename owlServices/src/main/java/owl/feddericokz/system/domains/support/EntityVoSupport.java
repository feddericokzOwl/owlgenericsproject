package owl.feddericokz.system.domains.support;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import owl.feddericokz.system.domains.EntityVo;
import owl.feddericokz.system.domains.ServiceReady;
import owl.feddericokz.system.domains.ServiceVo;
import owl.feddericokz.system.exceptions.NotImplementedException;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class EntityVoSupport<T extends ServiceReady<ID>,ID extends Serializable> implements EntityVo<T,ID> {

    /*
     * Fields
     */

    private ID id;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "uuuu-MM-dd'T'HH:mm:ss.SSSSSS")
    private LocalDateTime versionTag;

}

package owl.feddericokz.system.domains.support;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import owl.feddericokz.system.domains.ServiceReady;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@MappedSuperclass
@EqualsAndHashCode(callSuper = false)
@Setter
public abstract class ServiceReadySupport<ID extends Serializable> implements ServiceReady<ID> {

    /*
     * Fields
     */

    private ID id;
    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "uuuu-MM-dd'T'HH:mm:ss")
    private LocalDateTime versionTag;

    /*
     * Getters
     */

    @Override
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id") // todo Can this be taken from a properties file?
    public ID getId() {
        return this.id;
    }

    @Override
    @Version
    @Column(name = "last_updated") // todo Can this be taken from a properties file?
    public LocalDateTime getVersionTag() {
        return this.versionTag;
    }

    /*
     * Methods
     */

    @Override
    public abstract boolean autoValidate();

}

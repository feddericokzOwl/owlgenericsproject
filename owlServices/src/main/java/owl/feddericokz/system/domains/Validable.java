package owl.feddericokz.system.domains;

/**
 * This interface forces classes to know whether they are valid or not.
 */
public interface Validable {

    /**
     * @return this method should implement the logic to decide if the object is valid or not
     */
    boolean autoValidate();

}

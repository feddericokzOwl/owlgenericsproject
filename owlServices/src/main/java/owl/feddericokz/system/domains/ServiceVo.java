package owl.feddericokz.system.domains;

import owl.feddericokz.system.domains.support.EntityVoSupport;
import owl.feddericokz.system.domains.support.OwlCollectionsFactory;
import owl.feddericokz.system.domains.support.ServiceEnumSupport;
import owl.feddericokz.system.domains.support.ServiceReadySupport;
import owl.feddericokz.system.exceptions.InvalidEntityException;

import java.lang.reflect.*;
import java.util.Collection;
import java.util.stream.Collectors;

import static owl.feddericokz.system.utils.OwlReflectionUtils.implementsInterface;
import static owl.feddericokz.system.utils.OwlReflectionUtils.subclassOf;

/**
 * All Value Objects need to implement this interface to work with the GenericServices.
 * @param <T>
 */
public interface ServiceVo<T> {

    /**
     * Builds a Vo from an Entity Object.
     * @param entity
     * @return Returns a Vo representing the given Entity.
     */
    default ServiceVo fromDomain(T entity) {
        if (entity!=null) {
            // Now, if i have the instance, i need the fields of both classes to do the conversion.
            Field[] entityFields = this.getType().getDeclaredFields();
            for (Field entityField : entityFields) {
                int modifiers = entityField.getModifiers();
                if (Modifier.toString(modifiers).equals("public static final")
                        || Modifier.toString(modifiers).equals("private static final")) continue; // Constants should not be mapped.
                entityField.setAccessible(true);
                if (implementsInterface(entityField.getType(), Collection.class)) {
                    try {
                        Field collectionField = this.getClass().getDeclaredField(entityField.getName());
                        collectionField.setAccessible(true);
                        Collection<?> collection = (Collection<?>) entityField.get(entity);
                        // I have to stream this collection, check if it's a EntityVo or a EnumVo, and call to domain
                            if (collection != null && !collection.isEmpty())
                                collectionField.set(this,
                                        collection.stream().map(entityElement -> {
                                                if (implementsInterface(entityElement.getClass(), ServiceReady.class)) { // Check if it implements EntityVo

                                                    // Here  i need a new instance of voCollectionType.
                                                    Object newInstance = getNewServiceVoInstanceFromCollectionField(collectionField);
                                                    if (newInstance!=null)
                                                        return ((EntityVo) newInstance).fromDomain(entityElement);
                                                    else return null;

                                                } else if (implementsInterface(entityElement.getClass(), ServiceEnum.class)) { // Check if it implements EnumVo

                                                    // Here  i need a new instance of voCollectionType.
                                                    Object newInstance = getNewServiceVoInstanceFromCollectionField(collectionField);
                                                    if (newInstance!=null)
                                                        return ((EnumVo) newInstance).fromDomain(entityElement);
                                                    else return null;

                                                } else {
                                                    return entityElement;
                                                }
                                        }).collect(Collectors.toCollection(new OwlCollectionsFactory(collectionField)))
                                );
                    } catch (NoSuchFieldException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        Field voField = this.getClass().getDeclaredField(entityField.getName());
                        voField.setAccessible(true);
                        if (implementsInterface(voField.getType(), EntityVo.class) || subclassOf(voField.getType(), EntityVoSupport.class)) { // Check if it implements EntityVo
                            if (entityField.get(entity)!=null) {

                                Object newInstance = getNewServiceVoInstance(voField);

                                voField.set(this,
                                        // New entity
                                        ((EntityVo) newInstance).fromDomain(entityField.get(entity))
                                );
                            }
                        } else if (implementsInterface(voField.getType(), EnumVo.class)) { // Check if it implements EnumVo
                            if (entityField.get(entity)!=null) {

                                Object newInstance = getNewServiceVoInstance(voField);

                                voField.set(this,
                                        // New entity
                                        ((EnumVo) newInstance).fromDomain(entityField.get(entity))
                                );
                            }
                        } else {
                            voField.set(this, entityField.get(entity));
                        }
                    } catch (NoSuchFieldException | IllegalAccessException e) {
                        e.printStackTrace();
                        // todo Should do something here too?
                    }
                }
            }

            // If it's a subclass of of ServiceReadySupport i need to get the Id and versionTag too;
            if (subclassOf(entity.getClass(), ServiceReadySupport.class) || subclassOf(entity.getClass(), ServiceEnumSupport.class)) {
                Field[] serviceReadySupportFields = entity.getClass().getSuperclass().getDeclaredFields();
                for (Field serviceReadySupportField : serviceReadySupportFields) {
                    serviceReadySupportField.setAccessible(true);
                    try {
                        // That the source class is an instance of ServiceReadySupport, doesnt make the Vo an instance of EntityVoSupport
                        Field entityVoField;
                        if (subclassOf(this.getClass(), EntityVoSupport.class)) {
                            entityVoField = this.getClass().getSuperclass().getDeclaredField(serviceReadySupportField.getName());
                        } else {
                            entityVoField = this.getClass().getDeclaredField(serviceReadySupportField.getName());
                        }
                        entityVoField.setAccessible(true);
                        entityVoField.set(this,serviceReadySupportField.get(entity));
                    } catch (NoSuchFieldException | SecurityException | IllegalAccessException e) {
                        e.printStackTrace();
                        // todo Should do something?
                    }
                }
                return this;
            }

            // If it's an instance of ServiceReady, call autoValidate() before returning.
            // Set versionTag and Id fields.
            if (implementsInterface(entity.getClass(),ServiceReady.class)) {
                ServiceReady serviceReady = (ServiceReady) entity;
                if (subclassOf(this.getClass(), EntityVoSupport.class)) {
                    EntityVoSupport entityVoSupport = (EntityVoSupport) this;
                    entityVoSupport.setId(serviceReady.getId());
                    entityVoSupport.setVersionTag(serviceReady.getVersionTag());
                }
                //if (!serviceReady.autoValidate()) throw new InvalidEntityException(this.getType());
                return this;
            } else if (implementsInterface(entity.getClass(),ServiceEnum.class)) {
                ServiceEnum serviceEnum = (ServiceEnum) entity;
                if (subclassOf(this.getClass(), EntityVoSupport.class)) {
                    EntityVoSupport entityVoSupport = (EntityVoSupport) this;
                    entityVoSupport.setId(serviceEnum.getId());
                }
                return this;
            }
        }
        throw new IllegalStateException("Shuldnt reach here.");
    }

    /**
     * Builds an entity from the Vo instance.
     *
     * @return Returns an Entity object.
     */
    default T toDomain() {
        T entity = null;
        Constructor<T>[] constructors = (Constructor<T>[]) this.getType().getDeclaredConstructors();
        for (Constructor<T> constructor : constructors) {
            if (constructor.getParameterTypes().length==0) {
                try {
                    constructor.setAccessible(true);
                    entity = constructor.newInstance();
                } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                    // todo Should do something here?
                }
            }
        }

        if (entity!=null) {
            // Now, if i have the instance, i need the fields of both classes to do the conversion.
            Field[] entityFields = this.getType().getDeclaredFields();
            for (Field entityField : entityFields) {
                int modifiers = entityField.getModifiers();
                if (Modifier.toString(modifiers).equals("public static final")
                || Modifier.toString(modifiers).equals("private static final")) continue; // Constants should not be mapped.
                entityField.setAccessible(true);
                if (implementsInterface(entityField.getType(), Collection.class)) {
                    try {
                        Field voField = this.getClass().getDeclaredField(entityField.getName());
                        voField.setAccessible(true);
                        Collection<?> voCollection = (Collection<?>) voField.get(this);
                        // I have to stream this collection, check if it's a EntityVo or a EnumVo, and call to domain.
                        if (voCollection != null && !voCollection.isEmpty())
                            entityField.set(entity,
                                    voCollection.stream().map(o -> {
                                        /*try {*/
                                            if (implementsInterface(o.getClass(), EntityVo.class)) { // Check if it implements EntityVo
                                                return ((EntityVo) o).toDomain();
                                            } else if (implementsInterface(o.getClass(), EnumVo.class)) { // Check if it implements EnumVo
                                                return ((EnumVo) o).toDomain();
                                            } else {
                                                return o;
                                            }
                                    }).collect(Collectors.toCollection(new OwlCollectionsFactory(entityField)))
                            );
                    } catch (NoSuchFieldException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        Field voField = this.getClass().getDeclaredField(entityField.getName());
                        voField.setAccessible(true);
                        if (implementsInterface(voField.getType(), EntityVo.class) || subclassOf(voField.getType(),EntityVoSupport.class)) { // Check if it implements EntityVo
                            if (voField.get(this)!=null)
                                entityField.set(entity, ((EntityVo) voField.get(this)).toDomain());
                        } else if (implementsInterface(voField.getType(), EnumVo.class)) { // Check if it implements EnumVo
                            if (voField.get(this)!=null)
                                entityField.set(entity, ((EnumVo) voField.get(this)).toDomain());
                        } else {
                            entityField.set(entity,voField.get(this));
                        }
                    } catch (NoSuchFieldException | IllegalAccessException e) {
                        e.printStackTrace();
                        // todo Should do something here too?
                    }
                }
            }

            // If it's a subclass of of ServiceReadySupport i need to get the Id and versionTag too;
            if (subclassOf(entity.getClass(), ServiceReadySupport.class) || subclassOf(entity.getClass(), ServiceEnumSupport.class)) {
                Field[] serviceReadySupportFields = entity.getClass().getSuperclass().getDeclaredFields();
                for (Field serviceReadySupportField : serviceReadySupportFields) {
                    serviceReadySupportField.setAccessible(true);
                    try {
                        // That the source class is an instance of ServiceReadySupport, doesnt make the Vo an instance of EntityVoSupport
                        Field entityVoField;
                        if (subclassOf(this.getClass(), EntityVoSupport.class)) {
                            entityVoField = this.getClass().getSuperclass().getDeclaredField(serviceReadySupportField.getName());
                        } else {
                            entityVoField = this.getClass().getDeclaredField(serviceReadySupportField.getName());
                        }
                        entityVoField.setAccessible(true);
                        serviceReadySupportField.set(entity,entityVoField.get(this));
                    } catch (NoSuchFieldException | SecurityException | IllegalAccessException e) {
                        e.printStackTrace();
                        // todo Should do something?
                    }
                }
                return entity;
            }

            // Set versionTag and Id fields.
            if (implementsInterface(entity.getClass(),ServiceReady.class)) {
                ServiceReady serviceReady = (ServiceReady) entity;
                if (subclassOf(this.getClass(), EntityVoSupport.class)) {
                    EntityVoSupport entityVoSupport = (EntityVoSupport) this;
                    serviceReady.setId(entityVoSupport.getId());
                    serviceReady.setVersionTag(entityVoSupport.getVersionTag());
                }
                return entity;
            } else if (implementsInterface(entity.getClass(),ServiceEnum.class)) {
                ServiceEnum serviceEnum = (ServiceEnum) entity;
                if (subclassOf(this.getClass(), EntityVoSupport.class)) {
                    EntityVoSupport entityVoSupport = (EntityVoSupport) this;
                    serviceEnum.setId(entityVoSupport.getId());
                }
                return entity;
            }
        }
        throw new IllegalStateException("Shuldnt reach here.");
    }

    private Class<T> getType() {
        ParameterizedType type;
        try {
            // Case object extends EntityVoSupport/EnumVoSupport
            type = (ParameterizedType) this.getClass().getGenericSuperclass();
        } catch (ClassCastException e) {
            // Case object implements EntityVo/EnumVo
            type = (ParameterizedType) this.getClass().getGenericInterfaces()[0];
        }
        return (Class<T>) type.getActualTypeArguments()[0];
    }

    private Object getNewServiceVoInstanceFromCollectionField(Field collectionField) {
        ParameterizedType parameterizedType = (ParameterizedType) collectionField.getGenericType();
        Class<?> listType = (Class<?>) parameterizedType.getActualTypeArguments()[0];
        try {
            Constructor<?> listTypeConstructor = listType.getConstructor();
            return listTypeConstructor.newInstance();
        } catch (InstantiationException | InvocationTargetException | IllegalAccessException | NoSuchMethodException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Object getNewServiceVoInstance(Field voField) {
        Class<?> voClass = voField.getType();
        try {
            Constructor<?> listTypeConstructor = voClass.getConstructor();
            return listTypeConstructor.newInstance();
        } catch (InstantiationException | InvocationTargetException | IllegalAccessException | NoSuchMethodException e) {
            e.printStackTrace();
            return null;
        }
    }

}

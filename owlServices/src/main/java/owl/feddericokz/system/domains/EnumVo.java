package owl.feddericokz.system.domains;

import java.io.Serializable;

/**
 * Interface that must be implemented by DataTransferObjects which represent database enumerates
 * that intend to work with the System Generics.
 * @param <T>
 * @param <ID>
 */
public interface EnumVo<T extends ServiceEnum<ID>,ID extends Serializable> extends Identifiable<ID>, ServiceVo<T> {
}
